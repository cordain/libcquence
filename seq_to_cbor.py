#!/usr/bin/python3
try:
    from sys import argv
    from os import path
    from yaml import load
    from yaml import CLoader as Loader
except ImportError as e:
    print(e)
    exit(1)

STANDARD_REQUESTS = ["end_of_sequence", "receive", "branch", "send", "call"]

if len(argv) < 2:
    print("No arguments provided")
    exit(1)
if not path.isfile(argv[1]):
    print("File not found")
    exit(2)

seq_content:dict
with open(argv[1],"r") as f:
    seq_content = load(f,Loader=Loader)
if not "sequences" in seq_content:
    print("Wrong file format - Sequences section not found")
    exit(3)

for seq_name in seq_content["sequences"]:
    sequence = seq_content["sequences"][seq_name]
    
