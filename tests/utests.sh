#!/usr/bin/bash

exit_code=0
success=0
compiled=0

pushd tests
rm cester.h
wget --quiet https://github.com/exoticlibraries/libcester/releases/download/v0.4/cester.h
if [ -f cester.h ]; then
   # Build and run step
   mkdir build
   pushd build
   rm -R *
   echo "UNIT TEST REPORT" > ../test_report.txt
   #figlet -c -f small UNIT TEST REPORT > ../test_report.txt
   modules=(seqmgr engine)
   for module in ${modules[@]}; do
      mkdir $module
      pushd $module
      cmake -DCMAKE_C_COMPILER=gcc ../../../src/$module
      make -j8
      compiled=$?
      popd
      echo "================================================================================" >> ../test_report.txt
      echo "$module unit tests" >> ../test_report.txt
      #figlet -c -f small ${module^^} >> ../test_report.txt
      if [[ $compiled -eq 0 ]]; then
         ./$module/target/"$module"_utest --cester-noisolation >> ../test_report.txt
      else
         echo "NOT COMPILED" >> ../test_report.txt
      fi
   done
   popd
   #clear
   echo ""
   echo "================================================================================"
   cat test_report.txt
   echo "================================================================================"
   echo ""
   # General pass/fail step
   for module in ${modules[@]}; do
      if [[ -f ./build/$module/target/"$module"_utest ]]; then
         success=$(./build/$module/target/"$module"_utest --cester-noisolation | tail -n2 | grep FAILURE  | wc -l)
      else
         success=1
      fi
      exit_code=$((exit_code + success))
      echo $module:$exit_code
   done
else
   exit_code=1
fi
popd > /dev/null
exit $exit_code
