///=================================================================================================
/// \\file opcodes.h
///
/// This file contains cofiguration that describes used operations
/// Please respect sections that covers DO NOT CHANGE code
///=================================================================================================

#ifndef LIBCQUENCE_OPCODES_H
#define LIBCQUENCE_OPCODES_H

typedef struct {} Cquence_OpCode_OP1_Arg;
typedef struct {} Cquence_OpCode_USER_OP_COUNT_Arg;

///================================================================================================
/// \\enum  Config_OpCode
/// \\brief Identifiers of operation codes.
///
/// Defined opcodes need to be started from 0 and be left with no empty values,
/// otherwise library will not be working
///================================================================================================
typedef enum{
///=================================================================================================
/// USER OPCODES
   OP1,
///=================================================================================================

///=================================================================================================
/// DO NOT CHANGE(len,eos) - standard values used in library
   USER_OP_COUNT,
///=================================================================================================
}Cquence_OpCode;
#endif//LIBCQUENCE_OPCODES_H
