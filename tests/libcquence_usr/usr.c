
///=================================================================================================
/// \\file usr.c
///
/// This file contains static data of configuration, including opmeta storage and external definitions of operation functions
///=================================================================================================

#include "config.h"
#include "cquence_types.h"

///================================================================================================
/// \\typedef EXTERNOP
/// \\brief Macro helper for creating function prototypes
///================================================================================================
#define EXTERNOP(name) extern Cquence_OpResult Cquence_ ## name ## Operation(void*)

///================================================================================================
/// \\typedef OPMETA
/// \\brief Macro helper for creating Cquence_OpMeta instance in operation_metadata array
///================================================================================================
#define OPCALL(opimpl) Cquence_ ## opimpl ## Operation

///================================================================================================
/// USER OP IMPLEMENTATION PROTOTYPES
EXTERNOP(Op1);
EXTERNOP(OpBlocking);
EXTERNOP(OpBranching);
EXTERNOP(OpPostsend);
EXTERNOP(OpPresend);
///================================================================================================

const Cquence_OpImpl operation_calls[USER_OP_COUNT] = {
///================================================================================================
/// USER OP META LIST
OPCALL(Op1),
OPCALL(OpBlocking),
OPCALL(OpBranching),
OPCALL(OpPostsend),
OPCALL(OpPresend),
///================================================================================================
};

const char* operation_str[USER_OP_COUNT] = {
///================================================================================================
/// USER OP META LIST
"OP1",
"OP_BLOCKING",
"OP_BRANCHING",
"OP_POSTSEND",
"OP_PRESEND",
///================================================================================================
};
