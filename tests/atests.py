#!/usr/bin/python3
import os
import sys
import re
from subprocess import run, PIPE, DEVNULL, TimeoutExpired, CalledProcessError
import importlib.util
from time import sleep

status_line_regex = re.compile(r"[ \d]+: \w+ as \w+\[\d+\] \w{2,3} \w{4,5}",re.M)

def import_acceptance(example_name,example_dir):
    spec = importlib.util.spec_from_file_location('acceptance', f"{example_dir}/{example_name}/acceptance.py")
    module = importlib.util.module_from_spec(spec)
    sys.modules['acceptance'] = module
    spec.loader.exec_module(module)
    return module

example_dir = "{}/examples".format(os.path.realpath("."))
print("="*80)
print("ACCEPTANCE TESTS")
for example_name in os.listdir(example_dir):
    print("="*80)
    print(f"{example_name} acceptance test")
    print("="*80)
    example = import_acceptance(example_name,example_dir)
    try:
        run([f"{example_dir}/{example_name}/build.sh", "test"], stdout=DEVNULL, stderr=DEVNULL, check=True)
    except CalledProcessError:
        print("ERROR: example not compiled")
        exit(1)
    # expecting timeout of infinite process
    example_output:str
    try:
        command = f"{example_dir}/{example_name}/build/target/{example_name}"
        with open(f"{example_dir}/{example_name}/testin","rb") as ftestin:
            testin = ftestin.read()
        run(["stdbuf", "-oL" ,"-eL", command],stdout=PIPE,input=testin,timeout=example.execution_time)
        print("ERROR: exited abnormaly")
        exit(1)
    except TimeoutExpired as e:
        example_output = e.stdout.decode("Latin1")
    #grep $(status_line_regex) output
    output_filtered = "\n".join(re.findall(status_line_regex,example_output))
    print(output_filtered)
    test_result = example.execute(output_filtered)
    if not test_result:
        exit(1)
print("="*80)
print("ACCEPTANCE TESTS PASSED")
print("="*80)
exit(0)
