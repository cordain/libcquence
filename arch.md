# Stack overview
```plantuml
@startuml
package Application{
  node app1
  node app2
}
package Middle{
  node eCquence{
    portin Ecquence_NewSeq
    portin Ecquence_StopSeq
    portin Ecquence_RestartSeq
    portin Ecquence_DeletaSeq
  
    component OpEngine{
      port Opengine_GetSequence
      port Opengine_GetOperationMetadata
      port Opengine_GetSequenceMessage
      port Opengine_SendSequenceMessage
      port Opengine_ExecuteOperation
    }
    note top of Opengine_GetSequence: At this point OpEngine is spawned in new thread
    note right of Opengine_ExecuteOperation: This port will use pointer to current operation implementation\nThis path is     meant to be the only one for execution of operation\nPoints to folder instead of operation, because current address of    function is dynamic

    component Seqmgr{
      portin Seqmgr_SequenceRequest
      portin Seqmgr_SequenceRequest
      portout Seqmgr_SpawnSequence
      portout Seqmgr_HangSequence
      portout Seqmgr_KillSequence
      portout Seqmgr_SendSequenceMessage
    }

    component Rtoswrap{
      portin Rtoswrap_ThreadRequest
      portin Rtoswrap_HangRequest
      portin Rtoswrap_KillRequest
      portout Rtoswrap_NewThread
      portout Rtoswrap_HangThread
      portout Rtoswrap_KillThread
    }

    component Config{
      portout op1implptr
      portout op2implptr
      portout op3implptr
      portout Config_OpMetaOfIndex
    }
  }
  node operations_implementation{
    folder operations_code{
      control op1impl
      control op2impl
      control op3impl
    }
  }
}
package Drivers{
  node driver1{
    port driver1func1
    port driver1func2
    port driver1diag
  }
  node driver2{
    port driver2func1
    port driver2diag
  }
}

Seqmgr_SpawnSequence -down-> Rtoswrap_ThreadRequest
Seqmgr_HangSequence -down-> Rtoswrap_HangRequest
Seqmgr_KillSequence -down-> Rtoswrap_KillRequest
Rtoswrap_NewThread -> Opengine_GetSequence
Rtoswrap_HangThread -> OpEngine
Rtoswrap_KillThread -> OpEngine

op1implptr .down.> op1impl
op2implptr .down.> op2impl
op3implptr .down.> op3impl

Opengine_GetOperationMetadata <- Config_OpMetaOfIndex

Opengine_GetSequenceMessage <- Seqmgr_SendSequenceMessage

op1impl -down-> driver1func1
op1impl <- driver1diag
op2impl -down-> driver1func2
op2impl <- driver1diag
op3impl -down-> driver2func1
op3impl <- driver2diag

Application -down-> Ecquence_NewSeq
Application -down-> Ecquence_StopSeq
Application -down-> Ecquence_DeletaSeq
Application -down-> Ecquence_RestartSeq

Seqmgr_SequenceRequest <-down- Ecquence_NewSeq
Seqmgr_SequenceRequest <-down- Ecquence_StopSeq
Seqmgr_SequenceRequest <-down- Ecquence_DeletaSeq
Seqmgr_SequenceRequest <-down- Ecquence_RestartSeq

Opengine_SendSequenceMessage -> Seqmgr_SequenceRequest
Opengine_ExecuteOperation ..> operations_code
@enduml
```
## Engine

```plantuml
@startuml
component OpEngine{
  database Opengine_Sequence{
    portin Opengine_StoreSequence
    portout Opengine_QuerySequence
    json Opengine_SequencesView{
      "[0]":{
        "opcode": "op1",
        "arg1": "(void*)arg1",
        "arg2": "(void*)arg2"
      },
      "[1]":{
        "opcode": "op2",
        "arg1": "(void*)arg1",
        "arg2": "null"
      },
      "[2]":{
        "opcode": "op3",
        "arg1": "null",
        "arg2": "null"
      },
      "[3]":{
        "opcode": "eos",
        "arg1": "null",
        "arg2": "null"
      }
    }
  }
  note top of Opengine_Sequence: Stored for thread lifetime
  control Opengine_CallOperation
  port Opengine_GetSequence
  port Opengine_GetOperationMetadata
  port Opengine_GetSequenceMessage
  port Opengine_SendSequenceMessage
  port Opengine_ExecuteOperation

  Opengine_GetSequence -down-> Opengine_StoreSequence

  Opengine_CallOperation <- Opengine_QuerySequence
  Opengine_CallOperation -down-> Opengine_ExecuteOperation
  Opengine_CallOperation <- Opengine_GetOperationMetadata
  Opengine_CallOperation <- Opengine_GetSequenceMessage
  Opengine_CallOperation -> Opengine_SendSequenceMessage
}
@enduml
```

```plantuml
@startuml
entity GetSequence
database Sequence
control CallOperation
entity GetOperationMetadata
entity SendSequenceMessage
entity GetSequenceMessage
entity ExecuteOperation

GetSequence -> Sequence: seq1(op1,op2,br1,eos,op3,eos)
group basic operation
  Sequence <- CallOperation: QuerySequence(0)
  Sequence -> CallOperation: seq1(op1,arg0,arg1)
  CallOperation -> GetOperationMetadata: Config_GetOpMetaOfIndex(op1)
  CallOperation <- GetOperationMetadata: meta(op1ptr,nonblocking,nonbranch)
  CallOperation -> ExecuteOperation: op1ptr(arg0,arg1)
  activate ExecuteOperation
  ExecuteOperation -> ExecuteOperation: do_stuff
  CallOperation <- ExecuteOperation: return(ok,data)
  deactivate ExecuteOperation
  CallOperation -> SendSequenceMessage: Seqmgr_SequenceRequest(MSG_STATUS,0,ok,op1)
end

group sending operation (can send to himself)
  Sequence <- CallOperation: QuerySequence(1)
  Sequence -> CallOperation: seq1(op2,arg0,arg1)
  CallOperation -> GetOperationMetadata: Config_GetOpMetaOfIndex(op2)
  CallOperation <- GetOperationMetadata: meta(op2ptr,nonblocking,nonbranch)
  CallOperation -> ExecuteOperation: op2ptr(arg0,arg1)
  activate ExecuteOperation
  ExecuteOperation -> ExecuteOperation: do_stuff
  CallOperation <- ExecuteOperation: return(ok,data)
  deactivate ExecuteOperation
  CallOperation -> SendSequenceMessage: Seqmgr_SequenceRequest(MSG_SEND,seq1,seq1,data)
  CallOperation -> SendSequenceMessage: Seqmgr_SequenceRequest(MSG_STATUS,seq1,1,ok,op2)
end

group branch operation (will wait for branch message)
  Sequence <- CallOperation: QuerySequence(2)
  Sequence -> CallOperation: seq1(br1,arg0,arg1)
  CallOperation -> GetOperationMetadata: Config_GetOpMetaOfIndex(br1)
  CallOperation <- GetOperationMetadata: meta(null,blocking,branch)
  CallOperation -> GetSequenceMessage: from(seq1)
  activate GetSequenceMessage
  GetSequenceMessage -> GetSequenceMessage: await(from(seq1))
  CallOperation <- GetSequenceMessage: msg(seq1,data)
  deactivate GetSequenceMessage
  CallOperation -> CallOperation: setseqcount(4)
  CallOperation -> SendSequenceMessage: Seqmgr_SequenceRequest(MSG_STATUS,seq1,2,ok,br1)
end

group wait for
  Sequence <- CallOperation: QuerySequence(4)
  Sequence -> CallOperation: seq1(op3,arg0,arg1)
  CallOperation -> GetOperationMetadata: Config_GetOpMetaOfIndex(op3)
  CallOperation <- GetOperationMetadata: meta(op3ptr,blocking,branch)
  CallOperation -> GetSequenceMessage: from(seq2)
  activate GetSequenceMessage
  GetSequenceMessage -> GetSequenceMessage: await(from(seq2))
  CallOperation <- GetSequenceMessage: msg(seq2,data)
  deactivate GetSequenceMessage
  CallOperation -> ExecuteOperation: op3ptr(arg0,arg1)
  activate ExecuteOperation
  ExecuteOperation -> ExecuteOperation: do_stuff
  CallOperation <- ExecuteOperation: return(ok,data)
  deactivate ExecuteOperation
  CallOperation -> SendSequenceMessage: Seqmgr_SequenceRequest(MSG_STATUS,seq1,4,ok,op3)
end
group end of sequence
  Sequence <- CallOperation: QuerySequence(5)
  Sequence -> CallOperation: seq1(eos)
  GetSequence <- CallOperation: terminate()
end
@enduml
```

## Sequence Manager

```plantuml
@startuml
component Seqmgr{
  portin Seqmgr_SequenceRequest
  portin Seqmgr_SequenceRequest
  portout Seqmgr_SpawnSequence
  portout Seqmgr_HangSequence
  portout Seqmgr_KillSequence
  portout Seqmgr_SendSequenceMessage

  control Seqmgr_HandleSequence
  control Seqmgr_PubSubMessenger
  control Seqmgr_ProcessRequest

  database Seqmgr_SequencesStatus{
    portin Seqmgr_NewSequence
    portin Seqmgr_ModifySequence
    portin Seqmgr_DropSequence
  }

  queue Seqmgr_RequestQueue{
    portin Seqmgr_PushRequest
    portout Seqmgr_PopRequest
    json Seqmgr_RequestQueueView{
      "[0]":{
        "Ecquence_NewSeq":{
          "name": "seq3",
          "def": "(opreq*)data"
        }
      },
      "[1]":{
        "send": {
          "sender": "seq2",
          "recipent": "seq1",
          "content": "(void*)data"
        }
      },
      "[2]":{
        "opstatus": {
          "name": "seq2",
          "successful": true,
          "currop": "op3",
          "seqidx": "5"
        }
      },
      "[3]":{
        "pause": {
          "name": "seq2",
          "time": 10
        }
      },
      "[4]":{
        "kill": {
          "name": "seq1"
        }
      }
    }
  }

  Seqmgr_ProcessRequest <- Seqmgr_PopRequest
  Seqmgr_ProcessRequest -> Seqmgr_ModifySequence
  Seqmgr_ProcessRequest -> Seqmgr_HandleSequence
  Seqmgr_ProcessRequest -> Seqmgr_DropSequence
  Seqmgr_ProcessRequest -> Seqmgr_PubSubMessenger

  Seqmgr_SequenceRequest -> Seqmgr_PushRequest

  Seqmgr_PubSubMessenger -> Seqmgr_SendSequenceMessage

  Seqmgr_HandleSequence -> Seqmgr_NewSequence
  Seqmgr_HandleSequence -> Seqmgr_SpawnSequence
  Seqmgr_HandleSequence -> Seqmgr_HangSequence
  Seqmgr_HandleSequence -> Seqmgr_KillSequence
}
@enduml
```

```plantuml
@startuml
actor Cquence as user
control Opengine as engine
queue Seqmgr_RequestQueue as req
control Seqmgr_ProcessRequest as procreq
control Seqmgr_HandleSequence as seqhandle
control Seqmgr_PubSubMessenger as pubsubmsg
database Seqmgr_SequencesStatus as seqsts

group Spawn first sequence
  user -> req: Seqmgr_PushRequest(new, seq1)
  req <- procreq: Seqmgr_PopRequest()
  req -> procreq: pop(new, seq1)
  procreq -> seqhandle: Seqmgr_HandleSequence(new, seq1)
  seqhandle -> seqsts: Seqmgr_NewSequence(seq1)
  engine <- seqhandle: Seqmge_SpawnSequence(seq1)
end
req <- procreq: Seqmgr_PopRequest()
req -> procreq: pop(empty)

group Two requests at a time
  engine -> req: Seqmgr_PushRequest(status,seq1,0,op2,5)
  user -> req: Seqmgr_PushRequest(new, seq2)
  req <- procreq: Seqmgr_PopRequest()
  req -> procreq: pop(status,seq1,0,op2,5)
  procreq -> seqhandle: Seqmgr_HandleSequence(status,seq1,0,op2,5)
  seqhandle -> seqsts: Seqmgr_ModifySequence(seq1,0,op2,5)
  req <- procreq: Seqmgr_PopRequest()
  req -> procreq: pop(new, seq2)
  procreq -> seqhandle: Seqmgr_HandleSequence(new, seq2)
  seqhandle -> seqsts: Seqmgr_NewSequence(seq2)
  engine <- seqhandle: Seqmge_SpawnSequence(seq2)
end

group Message from seq2 to seq1
  engine -> req: Seqmgr_PushRequest(send,seq2,seq1,"content")
  engine -> req: Seqmgr_PushRequest(status,seq2,0,op4,6)
  req <- procreq: Seqmgr_PopRequest()
  req -> procreq: pop(send,seq2,seq1,"content")
  procreq -> pubsubmsg: Seqmgr_PubSubMessenger(seq2,seq1,"content")
  pubsubmsg -> engine: Seqmgr_SendSequenceMessage(seq2,"content")
  req <- procreq: Seqmgr_PopRequest()
  req -> procreq: pop(status,seq1,0,op2,5)
  procreq -> seqhandle: Seqmgr_HandleSequence(status,seq1,0,op2,5)
  seqhandle -> seqsts: Seqmgr_ModifySequence(seq1,0,op2,5)
end
@enduml
```

## Config

```plantuml
@startuml
component Config{
  portout op1implptr
  portout op2implptr
  portout op3implptr
  portout Config_OpMetaOfIndex
  database Config_Operations{
    port Config_OperationAt 
    json Config_OperationMetas{
      "op1": {
        "implementation": "op1impl",
        "blocking": false,
        "branch": false
      },
      "op2": {
        "implementation": "op2impl",
        "blocking": true,
        "branch": false
      },
      "op3": {
        "implementation": "op3impl",
        "blocking": false,
        "branch": false
      },
      "eos": {}
    }
    Config_OperationAt -> Config_OpMetaOfIndex
    Config_Operations .. op1implptr
    Config_Operations .. op2implptr
    Config_Operations .. op3implptr
  }
}
@enduml
```

## OS wrappers

```plantuml
@startuml
component Rtoswrap{
  portin Rtoswrap_ThreadRequest
  portin Rtoswrap_HangRequest
  portin Rtoswrap_KillRequest
  portout Rtoswrap_NewThread
  portout Rtoswrap_HangThread
  portout Rtoswrap_KillThread

  folder Rtoswrap_Wrappers

  Rtoswrap_ThreadRequest -> Rtoswrap_Wrappers
  Rtoswrap_HangRequest -> Rtoswrap_Wrappers
  Rtoswrap_KillRequest -> Rtoswrap_Wrappers
  Rtoswrap_Wrappers -> Rtoswrap_NewThread
  Rtoswrap_Wrappers -> Rtoswrap_KillThread
  Rtoswrap_Wrappers -> Rtoswrap_HangThread
}
@enduml
```
# Possible applications

## Typical single sequence execution
```plantuml
@startuml
actor "App" as user
queue "SeqmgrRequests" as req
control "Seqmgr" as seqmgr
collections "RtosWrap" as rtoswrp
control "Opengine" as engine
database "Config" as cfg
control "Operation" as op

user -> user: seq1(op1,op2,op1,op3,eos)
user -> req: Ecquence_NewSeq(seq1)
req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: sequence(seq1)
seqmgr -> rtoswrp: Rtoswrap_ThreadRequest(seq1)
rtoswrp -> engine: Rtoswrap_NewThread(seq1)
activate engine
loop for every sequence item
  engine -> cfg: Config_OpMetaOfIndex(seq1[i].op)
  engine <- cfg: meta(op)
  engine -> op: Opengine_ExecuteOperation(seq1[i].arg1,seq1[i].arg2)
  engine <- op: done(opstatus)
  engine -> req: Opengine_SendSequenceMessage(seqstatus(opstatus))
  req <- seqmgr: Seqmgr_PopRequest()
  req -> seqmgr: status(seq1)
end
engine -> req: Opengine_SendSequenceMessage(seqstatus(eos))
req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: endseq(seq1)
seqmgr -> rtoswrp: Rtoswrap_KillRequest(seq1)
rtoswrp -> engine: Rtoswrap_KillThread(seq1)
deactivate engine
@enduml
```

## Two sequences, first waits for second

Timing diagram
```plantuml
@startuml
binary "LED" as led
analog "Motor" as mot

@0
led is 1
mot is 0

@5000
led is 0
mot is 100

@5500
led is 1
@6000
led is 0
@6500
led is 1
@7000
led is 0
@7500
led is 1
@8000
led is 0
@8500
led is 1
@9000
led is 0
@9500
led is 1

@10000
led is 0
mot is 0

@enduml
```

```plantuml
@startuml
actor "App" as user
queue "SeqmgrRequests" as req
control "Seqmgr" as seqmgr
collections "RtosWrap" as rtoswrp
control "Opengine1" as engine1
control "Opengine2" as engine2
database "Config" as cfg
collections "Operation" as op

user -> user: seq1(wait(seq2),led_set,wait(seq2),led_blink,wait(seq2),led_set,eos)
user -> user: seq2(note(seq1),mot_mv,note(seq1),mot_mv,note(seq1),eos)
user -> req: Ecquence_NewSeq(seq1)
user -> req: Ecquence_NewSeq(seq2)

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: sequence(seq1)
seqmgr -> rtoswrp: Rtoswrap_ThreadRequest(seq1)
rtoswrp -> engine1: Rtoswrap_NewThread(seq1)
activate engine1

engine1 -> cfg: Config_OpMetaOfIndex(seq1[0].op)
engine1 <- cfg: meta(wait(seq2))
engine1 -> engine1: Opengine_GetSequenceMessage()
activate engine1

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: sequence(seq2)
seqmgr -> rtoswrp: Rtoswrap_ThreadRequest(seq1)
rtoswrp -> engine2: Rtoswrap_NewThread(seq1)
activate engine2

engine2 -> cfg: Config_OpMetaOfIndex(seq2[0].op)
engine2 <- cfg: meta(note(seq1))
req <- engine2: Opengine_SendSequenceMessage(seq2,seq1)
req <- engine2: Opengine_SendSequenceMessage(status(seq2,0,true,note(seq1)))

engine2 -> cfg: Config_OpMetaOfIndex(seq2[1].op)
engine2 <- cfg: meta(mot_mv)
engine2 -> op: Opengine_ExecuteOperation(seq2[1].arg1,seq2[1].arg2)
op -> op: mot_mv
activate op

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: send(seq2,seq1)
seqmgr -> engine1: msg(seq2)

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,0,true,note(seq1))
seqmgr -> seqmgr: store(seq2,0,true,note(seq1))

deactivate engine1
req <- engine1: Opengine_SendSequenceMessage(status(seq1,0,true,wait(seq2)))

engine1 -> cfg: Config_OpMetaOfIndex(seq1[1].op)
engine1 <- cfg: meta(led_set)
engine1 -> op: Opengine_ExecuteOperation(seq1[1].arg1,seq1[1].arg2)
req <- engine1: Opengine_SendSequenceMessage(status(seq1,1,true,led_set))

engine1 -> cfg: Config_OpMetaOfIndex(seq1[2].op)
engine1 <- cfg: meta(wait(seq2))
engine1 -> engine1: Opengine_GetSequenceMessage()
activate engine1

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,0,true,wait(seq1))
seqmgr -> seqmgr: store(seq1,0,true,wait(seq1))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,1,true,led_set)
seqmgr -> seqmgr: store(seq1,1,true,led_set)

deactivate op
req <- engine2: Opengine_SendSequenceMessage(status(seq2,1,true,mot_mv))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,1,true,mot_mv)
seqmgr -> seqmgr: store(seq2,1,true,mot_mv)

engine2 -> cfg: Config_OpMetaOfIndex(seq2[2].op)
engine2 <- cfg: meta(note(seq1))
req <- engine2: Opengine_SendSequenceMessage(seq2,seq1)
req <- engine2: Opengine_SendSequenceMessage(status(seq2,2,true,note(seq1)))

engine2 -> cfg: Config_OpMetaOfIndex(seq2[3].op)
engine2 <- cfg: meta(mot_mv)
engine2 -> op: Opengine_ExecuteOperation(seq2[3].arg1,seq2[3].arg2)
op -> op: mot_mv
activate op

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: send(seq2,seq1)
seqmgr -> engine1: msg(seq2)

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,0,true,note(seq1))
seqmgr -> seqmgr: store(seq2,2,true,note(seq1))

deactivate engine1
req <- engine1: Opengine_SendSequenceMessage(status(seq1,2,true,wait(seq2)))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,2,true,wait(seq1))
seqmgr -> seqmgr: store(seq1,2,true,wait(seq1))

engine1 -> cfg: Config_OpMetaOfIndex(seq1[3].op)
engine1 <- cfg: meta(led_blink)
engine1 -> op: Opengine_ExecuteOperation(seq1[3].arg1,seq1[3].arg2)
req <- engine1: Opengine_SendSequenceMessage(status(seq1,3,true,led_blink))

engine1 -> cfg: Config_OpMetaOfIndex(seq1[4].op)
engine1 <- cfg: meta(wait(seq2))
engine1 -> engine1: Opengine_GetSequenceMessage()
activate engine1

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,3,true,led_blink)
seqmgr -> seqmgr: store(seq1,3,true,led_blink)

deactivate op
req <- engine2: Opengine_SendSequenceMessage(status(seq2,3,true,mot_mv))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,3,true,mot_mv)
seqmgr -> seqmgr: store(seq2,3,true,mot_mv)

engine2 -> cfg: Config_OpMetaOfIndex(seq2[4].op)
engine2 <- cfg: meta(note(seq1))
req <- engine2: Opengine_SendSequenceMessage(seq2,seq1)
req <- engine2: Opengine_SendSequenceMessage(status(seq2,4,true,note(seq1)))

req <- engine2: Opengine_SendSequenceMessage(status(seq2,5,true,eos))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: send(seq2,seq1)
seqmgr -> engine1: msg(seq2)

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,4,true,note(seq1))
seqmgr -> seqmgr: store(seq2,4,true,note(seq1))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq2,5,true,eos)
seqmgr -> seqmgr: store(seq2,5,true,eos)
seqmgr -> engine2: kill()
deactivate engine2

deactivate engine1
req <- engine1: Opengine_SendSequenceMessage(status(seq1,4,true,wait(seq2)))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,4,true,wait(seq1))
seqmgr -> seqmgr: store(seq1,4,true,wait(seq1))

engine1 -> cfg: Config_OpMetaOfIndex(seq1[5].op)
engine1 <- cfg: meta(led_set)
engine1 -> op: Opengine_ExecuteOperation(seq1[5].arg1,seq1[5].arg2)
req <- engine1: Opengine_SendSequenceMessage(status(seq1,5,true,led_blink))

req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,5,true,led_set)
seqmgr -> seqmgr: store(seq1,5,true,led_set)

req <- engine1: Opengine_SendSequenceMessage(status(seq1,6,true,eos))
req <- seqmgr: Seqmgr_PopRequest()
req -> seqmgr: status(seq1,6,true,eos)
seqmgr -> seqmgr: store(seq1,6,true,eos)
seqmgr -> engine1: kill()
deactivate engine1
@enduml
```
