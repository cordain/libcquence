#!/usr/bin/python3
try:
    from yaml import load
    from yaml import CLoader as Loader
    from jinja2 import Environment, FileSystemLoader
    import os
    from sys import argv
except ImportError as e:
    print(e)
    exit(1)
LIBCQUENCE_PATH = os.path.dirname(os.path.abspath(argv[0]))
libdef=None
with open("libdef.yaml","r") as libdef_file:
    libdef = load(libdef_file,Loader=Loader)

jinja_env = Environment(loader=FileSystemLoader(f"{LIBCQUENCE_PATH}/src/libcquence_usr/"))

LIBCQUENCE_USR="libcquence_usr"
if not os.path.exists(LIBCQUENCE_USR):
    os.mkdir(LIBCQUENCE_USR)
else:
    os.remove(f"{LIBCQUENCE_USR}/opcodes.h")
    os.remove(f"{LIBCQUENCE_USR}/usr.c")
    os.remove(f"{LIBCQUENCE_USR}/sequences.c")
    if os.path.exists(f"{LIBCQUENCE_USR}/_opimpl.c"):
        os.remove(f"{LIBCQUENCE_USR}/_opimpl.c")
# check for existence of config.h file of libcquence_usr
# do not create new one if exists
if not os.path.exists(f"{LIBCQUENCE_USR}/config.h"):
    with open(f"{LIBCQUENCE_PATH}/src/libcquence_usr/config.h",'rb') as src:
        with open(f"{LIBCQUENCE_USR}/config.h",'wb') as dst:
            dst.write(src.read())

for filename in ["_opimpl.c","opcodes.h","usr.c","sequences.c"]:
    template = jinja_env.get_template(filename)
    rendered = template.render(libdef)
    with open(f"{LIBCQUENCE_USR}/{filename}",'w+') as file:
        file.write(rendered)
