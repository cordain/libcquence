///=================================================================================================
/// \file cquence_internal.h
///
/// This file represents internal api definitions
///=================================================================================================
#ifndef LIBCQUENCE_INTERNAL_H
#define LIBCQUENCE_INTERNAL_H

#ifndef UNIT_TESTS
#define CQUENCE_FOREVER while(true)
#define CQUENCE_TERMINATE() break
#define CQUENCE_PRIVATE_FN(type,name) static type name
#else
#define CQUENCE_FOREVER 
#define CQUENCE_PRIVATE_FN(type,name) type __real_ ## name
#define CQUENCE_TERMINATE() 
#endif

#endif//LIBCQUENCE_INTERNAL_H
