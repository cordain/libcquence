#include "cester.h"

#ifndef UTEST_ONCE
#define UTEST_ONCE
#include "engine.c"
static size_t handler_called = 0;
static size_t handler_call_coverage = 0;
enum{
STATUSSEND,
OPIMPLOFINDEX,
TERMINATESELF,
REQUESTDELAY,
ENGINEPAUSE,
HANDLECALLREQUEST,
ENGINEHANGUP,
HANDLESENDREQUEST,
HANDLERECEIVEREQUEST,
HANDLEBRANCHREQUEST,
ENGINEEXECUTE,
INVALID=0xFF
};
//https://stackoverflow.com/a/35156625
#include "bits.h"
Cquence_OpResult Cquence_Op1Operation(void* arg){
   return (Cquence_OpResult){.successful=true};
}
#endif

CESTER_MOCK_FUNCTION(CquenceCfg_Cquence_OpImplOfIndex(Cquence_OpCode op),Cquence_OpImpl,
   handler_called |= BIT(OPIMPLOFINDEX);
   return Cquence_Op1Operation;
)
CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_TaskTicks(),RtosWrp_TimeType,0)
CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_QueuePop(RtosWrp_QueueType queue, void* buffer, RtosWrp_TimeType delay),void*,0)
CESTER_MOCK_FUNCTION(RtosWrp_TerminateSelf(),void,
   handler_called |= BIT(TERMINATESELF);
)
CESTER_MOCK_FUNCTION(RequestDelay(DelayRequest* delay_request, EngineState state_context, size_t delay),void,
   handler_called |= BIT(REQUESTDELAY);
   __real_RequestDelay(delay_request, state_context, delay);
)
CESTER_MOCK_FUNCTION(EnginePause(Cquence_Sig* signal, DelayRequest* delay_request, RtosWrp_TimeType delta),void,
   handler_called |= BIT(ENGINEPAUSE);
   __real_EnginePause(signal,delay_request,delta);
)
CESTER_MOCK_FUNCTION(HandleCallRequest(const Cquence_OpRequestBodyCall* op_request_call),void,
   handler_called |= BIT(HANDLECALLREQUEST);
   __real_HandleCallRequest(op_request_call);
)
CESTER_MOCK_FUNCTION(EngineHangup(DelayRequest* delay_request),void,
   handler_called |= BIT(ENGINEHANGUP);
   __real_EngineHangup(delay_request);
)
CESTER_MOCK_FUNCTION(HandleSendRequest(const OpEngine_Params* engine_params, const Cquence_OpRequestBodySend* op_request_send),void,
   handler_called |= BIT(HANDLESENDREQUEST);
   __real_HandleSendRequest(engine_params, op_request_send);
)
CESTER_MOCK_FUNCTION(HandleReceiveRequest(DelayRequest* delay_request, const Cquence_Sig* signal, const Cquence_OpRequestBodyReceive* op_request_receive),void,
   handler_called |= BIT(HANDLERECEIVEREQUEST);
   __real_HandleReceiveRequest(delay_request, signal, op_request_receive);
)
CESTER_MOCK_FUNCTION(HandleBranchRequest(DelayRequest* delay_request, size_t* sequence_idx, const Cquence_OpRequestBodyBranch* op_request_branch, const Cquence_Sig* signal, const OpEngine_Params* engine_params),void,
   handler_called |= BIT(HANDLEBRANCHREQUEST);
   __real_HandleBranchRequest(delay_request, sequence_idx, op_request_branch, signal, engine_params);
)
CESTER_MOCK_FUNCTION(EngineExecute(const OpEngine_Params * engine_params, size_t *sequence_idx, Cquence_Sig* signal, DelayRequest* delay_request),void,
   handler_called |= BIT(ENGINEEXECUTE);
   __real_EngineExecute(engine_params,sequence_idx,signal,delay_request);
)
CESTER_MOCK_FUNCTION(ChangeState(const Cquence_Sig* const signal, EngineState* state),void,
   __real_ChangeState(signal,state);
      )
CESTER_MOCK_FUNCTION(QueueRead(RtosWrp_QueueType engine_queue, Cquence_Sig* receive, size_t delay),void,
   static bool next_terminates = false;
   switch(engine_queue){
   case 0:receive->sig_type = SIG_INVALID;receive->sender="";break;
   case 1: if(delay == SIZE_MAX){
      if(!next_terminates){
         receive->sig_type = SIG_RESUME;receive->sender="SEQ3";
         next_terminates = true;
      }
      else{
         receive->sig_type = SIG_KILL;
      }
   }
   else{
      next_terminates = false;
      receive->sig_type = SIG_INVALID;receive->sender="";
   } break;
   case 2:if(delay == SIZE_MAX){
      receive->sig_type = SIG_RESUME;receive->sender="SEQ1";
   }
   else{
      receive->sig_type = SIG_INVALID;receive->sender="";
   } break;
   case 3:if(delay > 0){
      receive->sig_type = SIG_RESUME;receive->sender="SEQ1";
   }
   else{
      receive->sig_type = SIG_INVALID;receive->sender="";
   } break;
   case 4:if(delay > 0){
      receive->sig_type = SIG_RESUME;receive->sender="SEQ4";
   }
   else{
      receive->sig_type = SIG_INVALID;receive->sender="";
   } break;
   case 5:if(delay == 0){
      if(!next_terminates){
         receive->sig_type = SIG_PAUSE;receive->sig_body.sig_pause.pause_for_ticks=SIZE_MAX;receive->sender="API";
      }
      else{
         receive->sig_type = SIG_INVALID;receive->sender="";
      }
   }
   else{
      receive->sig_type = SIG_RESUME;receive->sender="API";
   }
   break;
   case 6:if(delay == 0){
      if(!next_terminates){
         receive->sig_type = SIG_PAUSE;receive->sig_body.sig_pause.pause_for_ticks=100;receive->sender="API";
      }
      else{
         receive->sig_type = SIG_INVALID;receive->sender="";
      }
   }
   else{
      receive->sig_type = SIG_INVALID;receive->sender="";
   }
   break;

   }
)
CESTER_MOCK_FUNCTION(StatusSend(const char* name, RtosWrp_QueueType req_queue, uint16_t sequence_idx),void,
   handler_called |= BIT(STATUSSEND);
)

CESTER_TEST(test_execution_of_user_operation,inst,
   const Cquence_OpRequest seq[] = {
      CALL(OP1),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 0
   };

   CquenceEngine_Runtime(&params);

   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLECALLREQUEST,OPIMPLOFINDEX,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_send_operation,inst,
   const Cquence_OpRequest seq[] = {
      SEND("SEQ1"),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 0
   };

   CquenceEngine_Runtime(&params);

   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLESENDREQUEST,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_receive_operation_received_from_other_than_SEQ1,inst,
   const Cquence_OpRequest seq[] = {
      RECEIVE("SEQ1"),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 1
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLERECEIVEREQUEST,REQUESTDELAY,TERMINATESELF);
//-------------------------------------------------------------------------------------------------^^^^^^^^^^^^^^--
//                                                               used inside queue read to inject loop termination
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_receive_operation_received_from_SEQ1,inst,
   const Cquence_OpRequest seq[] = {
      RECEIVE("SEQ1"),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 2
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLERECEIVEREQUEST,REQUESTDELAY,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_positive_branch_condition_met,inst,
   const Cquence_OpRequest seq[] = {
      BRANCH (true,"SEQ1",1000U,2),
      END_OF_SEQUENCE(),
      CALL(OP1),
      END_OF_SEQUENCE(),
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 3
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLEBRANCHREQUEST,REQUESTDELAY,HANDLECALLREQUEST,OPIMPLOFINDEX,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_negative_branch_condition_met,inst,
   const Cquence_OpRequest seq[] = {
      BRANCH (false,"SEQ1",1000U,2),
      END_OF_SEQUENCE(),
      CALL(OP1),
      END_OF_SEQUENCE(),
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 4
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLEBRANCHREQUEST,REQUESTDELAY,HANDLECALLREQUEST,OPIMPLOFINDEX,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_positive_branch_condition_not_met,inst,
   const Cquence_OpRequest seq[] = {
      BRANCH (true,"SEQ1",1000U,2),
      END_OF_SEQUENCE(),
      CALL(OP1),
      END_OF_SEQUENCE(),
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 4
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLEBRANCHREQUEST,REQUESTDELAY,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_negative_branch_condition_not_met,inst,
   const Cquence_OpRequest seq[] = {
      BRANCH (false,"SEQ1",1000U,2),
      END_OF_SEQUENCE(),
      CALL(OP1),
      END_OF_SEQUENCE(),
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 3
   };

   CquenceEngine_Runtime(&params);
   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLEBRANCHREQUEST,REQUESTDELAY,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_send_operation_after_hangup,inst,
   const Cquence_OpRequest seq[] = {
      SEND("SEQ1"),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 5
   };

   CquenceEngine_Runtime(&params);

   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLESENDREQUEST,ENGINEHANGUP,REQUESTDELAY,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)
CESTER_TEST(test_execution_of_send_operation_after_pause,inst,
   const Cquence_OpRequest seq[] = {
      SEND("SEQ1"),
      END_OF_SEQUENCE()
   };
   OpEngine_Params params = {
      .name = "SEQ",
      .req_queue = 0,
      .sequence = seq,
      .my_queue = 6
   };

   CquenceEngine_Runtime(&params);

   size_t expected_handler_called = BITS(STATUSSEND,ENGINEEXECUTE,HANDLESENDREQUEST,ENGINEPAUSE,REQUESTDELAY,TERMINATESELF);
   cester_assert_equal(handler_called,expected_handler_called);
)

//EXECUTION_COVERAGE_HARNESS
CESTER_BEFORE_EACH(inst,name,index,
   handler_called = 0;
)
CESTER_AFTER_EACH(inst,name,index,
   handler_call_coverage |= handler_called;
)
CESTER_TEST(execution_coverage_test,inst,
   size_t expected_handler_called = 0b11111111111;
   printf("EXECUTION_COVERAGE: 0x%04x call mask expected, 0x%04x mask produced\n",expected_handler_called,handler_call_coverage);
   cester_assert_true(expected_handler_called == handler_call_coverage);
)
