#include "engine.h"
#include "cquence_types.h"
#include "seqmgr/seqmgr_message.h"
#include <stdint.h>
#include <string.h>
#include "config/config_if.h"
#include "rtoswrp/rtoswrp.h"

#include "cquence_internal.h"

typedef enum{
   ENGINE_EXECUTE,
   ENGINE_PAUSE,
   ENGINE_HANGUP,
   ENGINE_TERMINATE = 0xFF
} EngineState;

typedef struct{
   size_t delay;
   bool requested;
   EngineState context;
} DelayRequest;

typedef struct{
  EngineState state;
  Cquence_Sig last_signal;
  DelayRequest delay;
  size_t sequence_idx;
} OpEngine;

/*Opengine_SendSequenceMessage*/
static void SendSequenceMessage(SeqMgr_Message seq_msg, RtosWrp_QueueType req_queue ){
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(req_queue, (void*)&seq_msg, 100));
}

CQUENCE_PRIVATE_FN(void,QueueRead)(RtosWrp_QueueType engine_queue, Cquence_Sig* receive, size_t delay){
   if (!RtosWrp_QueuePop(engine_queue, (void*)receive, delay)){
      receive->sig_type = SIG_INVALID;
   }
}

CQUENCE_PRIVATE_FN(void,RequestDelay)(DelayRequest* delay_request, EngineState state_context, size_t delay){
   delay_request->context = state_context;
   delay_request->delay = delay;
   delay_request->requested = true;
}

CQUENCE_PRIVATE_FN(void,StatusSend)(
   const char* name,
   RtosWrp_QueueType req_queue,
   uint16_t sequence_idx
){
   const SeqMgr_Message seq_sts = {
      .msg_type = MSG_STATUS,
      .msg_body = (SeqMgr_MsgBody){
         .msg_status = (SeqMgr_BodyMsgStatus){
            .name = name,
            .seqidx = sequence_idx, 
            .timestamp = RtosWrp_TaskTicks(),
         }
      }
   };
   SendSequenceMessage(seq_sts,req_queue);
}

CQUENCE_PRIVATE_FN(void,EnginePause)(Cquence_Sig* signal, DelayRequest* delay_request, RtosWrp_TimeType delta){
   if(delay_request->context == ENGINE_PAUSE && delay_request->requested && delta < delay_request->delay){
      RequestDelay(delay_request, ENGINE_PAUSE, delay_request->delay-delta);
   }
   else{
      RequestDelay(delay_request, ENGINE_PAUSE, signal->sig_body.sig_pause.pause_for_ticks);
   }
}

CQUENCE_PRIVATE_FN(void,HandleCallRequest)(const Cquence_OpRequestBodyCall* op_request_call){
   const Cquence_OpImpl current_operation = CquenceCfg_Cquence_OpImplOfIndex(op_request_call->operation);
   CQUENCE_ASSERT(current_operation != NULL);
   current_operation(op_request_call->arg);
}

CQUENCE_PRIVATE_FN(void,EngineHangup)(DelayRequest* delay_request){
   RequestDelay(delay_request, ENGINE_PAUSE, RTOSWRP_INFINITE_DELAY);
}

CQUENCE_PRIVATE_FN(void,HandleSendRequest)(const OpEngine_Params* engine_params, const Cquence_OpRequestBodySend* op_request_send){
   const SeqMgr_Message seq_signal = {
      .msg_type = MSG_SIGNAL,
      .msg_body = (SeqMgr_MsgBody){
         .msg_signal = (Cquence_Sig){
            .sender = engine_params->name,
            .recipent = op_request_send->recipent,
            .sig_type = SIG_RESUME,
            .sig_body = (Cquence_SigBody){
               .sig_resume = (Cquence_BodySigResume){
                  .resume_in_ticks = 0
               }
            }
         }
      }
   };
   SendSequenceMessage(seq_signal,engine_params->req_queue);
}

CQUENCE_PRIVATE_FN(void,HandleReceiveRequest)(DelayRequest* delay_request, const Cquence_Sig* signal, const Cquence_OpRequestBodyReceive* op_request_receive){
   if (delay_request->context == ENGINE_EXECUTE && delay_request->requested){
      delay_request->requested = false;
      if (signal->sig_type != SIG_RESUME && strcmp(signal->sender,op_request_receive->sender)) 
         RequestDelay(delay_request, ENGINE_EXECUTE, RTOSWRP_INFINITE_DELAY);
   }
   else{
      RequestDelay(delay_request, ENGINE_EXECUTE, RTOSWRP_INFINITE_DELAY);
   }
}

CQUENCE_PRIVATE_FN(void,HandleBranchRequest)(DelayRequest* delay_request, size_t* sequence_idx, const Cquence_OpRequestBodyBranch* op_request_branch, const Cquence_Sig* signal, const OpEngine_Params* engine_params){
   if (delay_request->context == ENGINE_EXECUTE && delay_request->requested){
      delay_request->requested = false;
      StatusSend(engine_params->name, engine_params->req_queue, *sequence_idx);
      if (op_request_branch->positive == (signal->sig_type == SIG_RESUME && !strcmp(signal->sender,op_request_branch->sender))) {
         (*sequence_idx) = op_request_branch->jump_to;
      }
      else{
         (*sequence_idx)++;
      }

   }
   else{
      RequestDelay(
         delay_request, 
         ENGINE_EXECUTE, 
         op_request_branch->wait_for_ticks
      );
   }
}

CQUENCE_PRIVATE_FN(void,EngineExecute)(const OpEngine_Params * engine_params, size_t *sequence_idx, Cquence_Sig* signal, DelayRequest* delay_request){
   Cquence_OpRequest current_request;
   size_t current_seq_idx = *sequence_idx;

   //Opengine_QuerySequence
   current_request = engine_params->sequence[current_seq_idx];
   switch(current_request.op_request_type){
      case OP_REQUEST_END_OF_SEQUENCE:
         *sequence_idx = SIZE_MAX-1;
         break;

      case OP_REQUEST_CALL:
         HandleCallRequest(&current_request.op_request_body.op_request_call);
         break;

      case OP_REQUEST_SEND:
         HandleSendRequest(engine_params,&current_request.op_request_body.op_request_send);
         break;

      case OP_REQUEST_RECEIVE:
         HandleReceiveRequest(delay_request,signal,&current_request.op_request_body.op_request_receive);
         if(delay_request->requested)
            return;
         break;

      case OP_REQUEST_BRANCH:
         HandleBranchRequest(delay_request,sequence_idx,&current_request.op_request_body.op_request_branch,signal,engine_params);
         return;

      default:
         break;
   }

   StatusSend(engine_params->name, engine_params->req_queue, current_seq_idx);
   (*sequence_idx)++;


   //
}

CQUENCE_PRIVATE_FN(void,ChangeState)(const Cquence_Sig* const signal, EngineState* state){
   if (signal->sig_type == SIG_KILL) 
      *state = ENGINE_TERMINATE;
   if (*state != ENGINE_TERMINATE){
      if (signal->sig_type == SIG_INVALID && *state != ENGINE_HANGUP){
         *state = ENGINE_EXECUTE;
      }
      else if (signal->sig_type == SIG_PAUSE && *state != ENGINE_HANGUP){
         if (signal->sig_body.sig_pause.pause_for_ticks == RTOSWRP_INFINITE_DELAY){
            *state = ENGINE_HANGUP;
         }
         else{
            *state = ENGINE_PAUSE;
         }
      }
      else if (signal->sig_type == SIG_RESUME && *state != ENGINE_PAUSE){
         *state = ENGINE_EXECUTE;
      }
      else {
         //do not change state
      }
   }
}

void CquenceEngine_Runtime(OpEngine_Params* engine_params){
   OpEngine engine = {
      .state = ENGINE_EXECUTE,
      .delay = {0},
      .sequence_idx = 0,
      .last_signal = {0}
   };
   OpEngine_Params my_params;
   memcpy(&my_params,engine_params,sizeof(OpEngine_Params));

   do{
      RtosWrp_TimeType delta = RtosWrp_TaskTicks();
      QueueRead(my_params.my_queue, 
         &engine.last_signal, 
         engine.delay.requested ? engine.delay.delay : 0
      );
      delta = RtosWrp_TaskTicks() - delta;
      ChangeState(&engine.last_signal,&engine.state);
      switch(engine.state){
      case ENGINE_EXECUTE:
         EngineExecute(&my_params, &engine.sequence_idx, &engine.last_signal, &engine.delay);
         break;
      case ENGINE_PAUSE:
         EnginePause(&engine.last_signal, &engine.delay, delta);
         break;
      case ENGINE_HANGUP:
         EngineHangup(&engine.delay);
         break;
      case ENGINE_TERMINATE:
         engine.sequence_idx = SIZE_MAX;
      default:
         break;
      }
   }while(engine.sequence_idx != SIZE_MAX);
   
   RtosWrp_TerminateSelf();
}
