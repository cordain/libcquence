///=================================================================================================
/// \file engine.h
///
/// Global functions of sequence engine. 
///=================================================================================================
#ifndef LIBCQUENCE_ENGINE_H
#define LIBCQUENCE_ENGINE_H

#include "cquence_types.h"
#include "rtoswrp/rtoswrp.h"
#include <stdint.h>


///=================================================================================================
/// \struct OpEngine_Params
///=================================================================================================
#define OpEngine_Params RtosWrp_EngineParams

void CquenceEngine_Runtime(OpEngine_Params* engine_params);
#endif//LIBCQUENCE_ENGINE_H
