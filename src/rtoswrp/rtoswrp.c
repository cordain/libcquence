///=================================================================================================
/// \file rtoswrp.c
///
/// This file contains os wrappers implementation
///=================================================================================================

#include "rtoswrp.h"
#include "engine/engine.h"
#include "seqmgr/seqmgr.h"
#include "seqmgr/seqmgr_message.h"

#if defined (USE_FREERTOS_POSIX)
void RtosWrp_ThreadRequest(RtosWrp_EngineParams* params){
   xTaskCreate((TaskFunction_t)CquenceEngine_Runtime,"TEST",1024,(OpEngine_Params*)params,tskIDLE_PRIORITY+1,NULL);
}

void RtosWrp_SpawnSeqMgr(RtosWrp_QueueType req_queue){
   xTaskCreate((TaskFunction_t)SeqMgr_Runtime,"SEQMGR",1024,req_queue,tskIDLE_PRIORITY+1,NULL);
}

void RtosWrp_TerminateSelf(){
   vTaskDelete( NULL );
}

RtosWrp_QueueType RtosWrp_SeqmgrQueueInit(size_t queue_size){
   return xQueueCreate(queue_size,sizeof(SeqMgr_Message));
}

RtosWrp_QueueType RtosWrp_EngineQueue(size_t queue_size){
   return xQueueCreate(queue_size,sizeof(Cquence_Sig));
}
void RtosWrp_DeleteQueue(RtosWrp_QueueType queue){
   vQueueDelete(queue);
}

void* RtosWrp_QueuePop(RtosWrp_QueueType queue, void* buffer, RtosWrp_TimeType delay){
   if (pdTRUE == xQueueReceive(queue,buffer,delay)){
      return buffer;
   }
   else{
      return NULL;
   }
}

bool RtosWrp_QueuePush(RtosWrp_QueueType queue, const void* data, RtosWrp_TimeType delay){
   return (bool)xQueueSend(queue,data,delay);
}

RtosWrp_TimeType RtosWrp_TaskTicks(){
   return xTaskGetTickCount();
}

#endif
