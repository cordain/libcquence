///=================================================================================================
/// \file rtoswrp.h
///
/// This file contains wrappers for different OS that this lib is used in
///=================================================================================================
#ifndef LIBSQUENCE_RTOSWRP_H
#define LIBSQUENCE_RTOSWRP_H

#include "cquence_types.h"
#include "config/config_os.h"
#include <stddef.h>
#include <stdint.h>

#if defined (USE_FREERTOS_POSIX)
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define RTOSWRP_INFINITE_DELAY portMAX_DELAY

typedef QueueHandle_t RtosWrp_QueueType;
typedef TickType_t RtosWrp_TimeType;
#elif defined (USE_UNIT_TEST)
#define RTOSWRP_INFINITE_DELAY SIZE_MAX

typedef unsigned RtosWrp_QueueType;
typedef unsigned RtosWrp_TimeType;
#endif

///=================================================================================================
/// \struct OpEngine_Params
///=================================================================================================
typedef struct{
   const char* name;
   RtosWrp_QueueType req_queue;
   const Cquence_OpRequest* sequence;
   RtosWrp_QueueType my_queue;
   size_t my_queue_size;
} RtosWrp_EngineParams;

///=================================================================================================
/// \fn RtosWrp_ThreadRequest
/// \brief Wrapper for spawning a thread
///=================================================================================================
void RtosWrp_ThreadRequest(RtosWrp_EngineParams* params);

void RtosWrp_SpawnSeqMgr(RtosWrp_QueueType queue);

void RtosWrp_TerminateSelf();

RtosWrp_TimeType RtosWrp_TaskTicks();

RtosWrp_QueueType RtosWrp_SeqmgrQueueInit(size_t queue_size);
RtosWrp_QueueType RtosWrp_EngineQueue(size_t queue_size);
void RtosWrp_DeleteQueue(RtosWrp_QueueType queue);
void* RtosWrp_QueuePop(RtosWrp_QueueType queue, void* buffer, RtosWrp_TimeType delay);
bool RtosWrp_QueuePush(RtosWrp_QueueType queue, const void* data, RtosWrp_TimeType delay);
#endif//LIBSQUENCE_RTOSWRP_H
