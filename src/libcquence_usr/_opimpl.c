///=================================================================================================
/// \file opimpl.c
///
/// This file contains implementations of operations.
/// This file is completely for user usage. It may be ommited and be defined somewhere else.
///=================================================================================================

#include "cquence_types.h"

///=================================================================================================
/// USER OP IMPLEMENTATION
{%- for opcode in operations %}
Cquence_OpResult Cquence_{{ opcode | replace("_"," ") | title | replace(" ","") }}Operation(void* arg){
   Cquence_OpCode_{{opcode | upper | replace(" ","")}}_Arg* data = arg;
   return (Cquence_OpResult){.successful=true};
}
{% endfor %}
///=================================================================================================
