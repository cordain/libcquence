#include "cquence_types.h"
#include "opcodes.h"

{%- if operations is defined %}
{%- if sequences is defined %}
{#- CONSTRUCT SEQUENCE NAMES #}
{%- for seq in sequences %}
#define {{seq | upper}}_NAME "{{seq}}"
{%- endfor %}
{% for seq in sequences %}
{#- FIRST PASS CONSTRUCTS LABEL INDEXES #}
{%- set label_dict = {}%}
{%- for request in sequences[seq] %}
{%- if 'label' in request %}
{%- set _ = label_dict.update({request.label: loop.index0}) %}
{%- endif %}
{%- endfor %}
{#- SECOND PASS CONSTRUCTS ACTUAL SEQUENCES #}
const Cquence_OpRequest {{seq}}[] = {
{%- for request in sequences[seq] %}
{%- if "receive" in request.request %}
{%- if request.from in sequences %}
    RECEIVE({{request.from | upper}}_NAME),
{%- endif %}
{%- elif "send" in request.request %}
{%- if request.to in sequences %}
    SEND({{request.to | upper}}_NAME),
{%- endif %}
{%- elif "branch" in request.request %}
{%- set strategy = "positive" in request.strategy %}
{%- if request.sender in sequences %}
    BRANCH({{strategy | lower}},{{request.sender | upper}}_NAME,{{request.timeout}},{{label_dict[request.jump_to]}}),
{%- endif %}
{%- elif "wait" in request.request %}
    BRANCH(false,"",{{request.for}},{{loop.index}}),
{%- elif "end_of_sequence" in request.request %}
    END_OF_SEQUENCE(),
{%- elif request.request in operations %}
{%- set separator = ', ' %}
{%- set joined_pairs = [] %}
    CALL({{request.request | upper}},
{%- for key, value in request.items()%}
{%- if key in operations[request.request] and not "label" in key %}
{%- if "char" in operations[request.request][key]%}
{%- if "*" in operations[request.request][key]%}
        .{{key}}="{{value}}",
{%- else %}
        .{{key}}='{{value}}',
{%- endif %}
{%- else %}
        .{{key}}={{value}},
{%- endif %}
{%- endif%}
{%-endfor%}
    ),
{%- endif %}
{%- endfor %}
{%- if not 'end_of_sequence' in sequences[seq][-1].request %}
    END_OF_SEQUENCE()
{%- endif %}
};
{% endfor %}
{%- endif %}
{%- endif %}
