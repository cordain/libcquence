///=================================================================================================
/// \\file opcodes.h
///
/// This file contains cofiguration that describes used operations
/// Please respect sections that covers DO NOT CHANGE code
///=================================================================================================

#ifndef LIBCQUENCE_OPCODES_H
#define LIBCQUENCE_OPCODES_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

{%- for opcode in operations %}
typedef struct{
{%- if operations[opcode] %}
{%- for arg in operations[opcode] %}
{%- if operations[opcode][arg].find("fn") != -1 %}
{%-set fn = operations[opcode][arg].split("(")%}
   {{fn[1][:-1]}}(*{{arg}})({{fn[2][:-1]}});
{%- else %}
   {{operations[opcode][arg]}} {{arg}};
{%- endif %}
{%- endfor %}
{%- endif %}
} Cquence_OpCode_{{opcode | upper | replace(" ","")}}_Arg;
{% endfor %}

///================================================================================================
/// \\enum  Config_OpCode
/// \\brief Identifiers of operation codes.
///
/// Defined opcodes need to be started from 0 and be left with no empty values,
/// otherwise library will not be working
///================================================================================================
typedef enum{
///=================================================================================================
/// USER OPCODES
{%- for opcode in operations %}
   {{opcode|upper|replace(" ","_")}},
{%- endfor %}
///=================================================================================================

///=================================================================================================
/// DO NOT CHANGE(len,eos) - standard values used in library
   USER_OP_COUNT,
///=================================================================================================
}Cquence_OpCode;
#endif//LIBCQUENCE_OPCODES_H
