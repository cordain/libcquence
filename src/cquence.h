///=================================================================================================
/// \file cquence.h
///
/// Global cquence interfaces
///=================================================================================================
#ifndef LIBSQUENCE_IF_H
#define LIBSQUENCE_IF_H

#include "cquence_types.h"
#include <stddef.h>

///=================================================================================================
/// \fn Cquence_NewSequence
/// \brief Interface for creating new sequence 
///=================================================================================================
void Cquence_NewSequence(const char* name, const Cquence_OpRequest* msg, size_t queue_size);

void Cquence_Init();

void Cquence_PauseSequence(const char* seq_name, size_t ticks);

void Cquence_ResumeSequence(const char* seq_name, size_t delay);

void Cquence_KillSequence(const char* seq_name, size_t delay);

void Cquence_ReplaceArgument(const char* seq_name, size_t index, void* argument, size_t argument_size);

#endif//LIBSQUENCE_IF_H
