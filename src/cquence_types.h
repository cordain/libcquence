///=================================================================================================
/// \file cquence_types.h
///
/// This file represents standard types used by LibCquence
///=================================================================================================
#ifndef LIBCQUENCE_TYPES_H
#define LIBCQUENCE_TYPES_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include "opcodes.h"
#include <assert.h>

///=================================================================================================
/// \typedef NULL
/// \brief nullpointer
///=================================================================================================
#ifdef NULL
#undef NULL
#endif
#define NULL (void*)0u

#ifndef UNIT_TESTS
#define CQUENCE_ASSERT(expr) assert((expr))
#else
#define CQUENCE_ASSERT(expr)
#endif

#define END_OF_SEQUENCE() (Cquence_OpRequest){\
      .op_request_type = OP_REQUEST_END_OF_SEQUENCE,\
      .op_request_body = {0}\
   }

#define RECEIVE(from) (Cquence_OpRequest){\
      .op_request_type = OP_REQUEST_RECEIVE,\
      .op_request_body = {\
         .op_request_receive = {\
            .sender = (from)\
         },\
      },\
   }

#define SEND(to) (Cquence_OpRequest){\
      .op_request_type = OP_REQUEST_SEND,\
      .op_request_body = {\
         .op_request_send = {\
            .recipent = (to)\
         },\
      },\
   }

#define BRANCH(strategy,send_from,delay,idx) (Cquence_OpRequest){\
      .op_request_type = OP_REQUEST_BRANCH,\
      .op_request_body = {\
         .op_request_branch = {\
            .sender = (send_from),\
            .positive = (strategy),\
            .wait_for_ticks = (delay),\
            .jump_to = (idx),\
         },\
      },\
   }

#define CALL(opcode,...) (Cquence_OpRequest){\
      .op_request_type = OP_REQUEST_CALL,\
      .op_request_body = {\
         .op_request_call = {\
            .operation = (opcode),\
            .arg=(void*)&(Cquence_OpCode_##opcode##_Arg){\
               __VA_ARGS__\
            }\
         },\
      },\
   }

///=================================================================================================
/// \struct Cquence_BodySigModSeq
///=================================================================================================
typedef struct{
   size_t resume_in_ticks;
} Cquence_BodySigResume;

///=================================================================================================
/// \struct Cquence_BodySigPause
///=================================================================================================
typedef struct{
   size_t pause_for_ticks;
} Cquence_BodySigPause;

///=================================================================================================
/// \struct Cquence_BodySigPause
///=================================================================================================
typedef struct{
   size_t kill_in_ticks;
} Cquence_BodySigKill;

///=================================================================================================
/// \struct Cquence_BodySigModSeq
///=================================================================================================
typedef struct{
   size_t seq_idx;
   void* data;
   size_t data_len;
} Cquence_BodySigModSeq;

///=================================================================================================
/// \union Cquence_SigBody
///=================================================================================================
typedef union{
   Cquence_BodySigResume sig_resume;
   Cquence_BodySigPause sig_pause;
   Cquence_BodySigKill sig_kill;
   Cquence_BodySigModSeq sig_mod_seq;
} Cquence_SigBody;

///=================================================================================================
/// \union Cquence_SigType
///=================================================================================================
typedef enum{
   SIG_INVALID=0,
   SIG_RESUME,
   SIG_PAUSE,
   SIG_KILL,
   SIG_MOD_SEQ,
} Cquence_SigType;

///=================================================================================================
/// \struct Cquence_Sig
///=================================================================================================
typedef struct{
   const char* sender;
   const char* recipent;
   Cquence_SigType sig_type;
   Cquence_SigBody sig_body;
} Cquence_Sig;

///=================================================================================================
/// \struct Cquence_OpRequest
/// \brief Request of the operation with arguments
///
/// Both arguments are used inside operation implementation
///=================================================================================================
typedef struct{
   void* arg;
   Cquence_OpCode operation;
} Cquence_OpRequestBodyCall;

typedef struct{
   const char* recipent;
} Cquence_OpRequestBodySend;

typedef struct{
   const char* sender;
} Cquence_OpRequestBodyReceive;

typedef struct{
   const char* sender;
   bool positive;
   size_t wait_for_ticks;
   size_t jump_to;
} Cquence_OpRequestBodyBranch;

typedef struct{} Cquence_OpRequestBodyEndOfSequence;

typedef union{
   Cquence_OpRequestBodyCall op_request_call;
   Cquence_OpRequestBodySend op_request_send;
   Cquence_OpRequestBodyReceive op_request_receive;
   Cquence_OpRequestBodyBranch op_request_branch;
   Cquence_OpRequestBodyEndOfSequence op_request_end_of_sequence;
} Cquence_OpRequestBody;

typedef enum{
   OP_REQUEST_CALL,
   OP_REQUEST_SEND,
   OP_REQUEST_RECEIVE,
   OP_REQUEST_BRANCH,
   OP_REQUEST_END_OF_SEQUENCE
} Cquence_OpRequestType;

typedef struct{
   Cquence_OpRequestType op_request_type;
   Cquence_OpRequestBody op_request_body;
} Cquence_OpRequest;

///=================================================================================================
/// \struct Cquence_OpResult
/// \brief Result of operation
///
/// If operation is successful, this result will have operation related data
/// Else result will have error info.
///=================================================================================================
typedef struct{
  bool successful;
  void* data;
} Cquence_OpResult;

///=================================================================================================
/// \typedef OpPtr
/// \brief Standard pointer to an operation.
///
/// \param[in] arg1
/// \param[in] arg2
///
/// \return Cquence_OpResult
///=================================================================================================
typedef Cquence_OpResult (*Cquence_OpImpl) (void*);

///=================================================================================================
/// \struct Cquence_OpMeta
/// \brief  Structure that contains metadata needed by sequence engine
///         to properly handle an operation
/// 
///=================================================================================================

#endif//LIBCQUENCE_TYPES_H
