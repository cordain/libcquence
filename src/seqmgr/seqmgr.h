///=================================================================================================
/// \file seqmgr.h
///
/// This file represents interfaces of seqmgr
///=================================================================================================
#ifndef LIBSQUENCE_SEQMGR_H
#define LIBSQUENCE_SEQMGR_H

#include "rtoswrp/rtoswrp.h"

///=================================================================================================
/// \fn Seqmgr_Runtime
/// \brief Thread interface to be called by OS
///=================================================================================================
void SeqMgr_Runtime(RtosWrp_QueueType req_queue);

#endif//LIBSQUENCE_SEQMGR_H
