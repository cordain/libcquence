///=================================================================================================
/// \file seqmgr_message.h
///
/// This file represents message types understandable by SeqMgr
///=================================================================================================

#include "cquence_types.h"
#include <stddef.h>
#include <stdint.h>

///=================================================================================================
/// \struct SeqMgr_BodySeqNew
///=================================================================================================
typedef struct{
   const char* name;
   const Cquence_OpRequest* const sequence;
   size_t queue_size;
} SeqMgr_BodySeqNew;

///=================================================================================================
/// \struct SeqMgr_BodySeqPause
///=================================================================================================
typedef struct{
   const char* name;
   uint32_t time;
} SeqMgr_BodySeqPause;

///=================================================================================================
/// \struct SeqMgr_BodySeqKill
///=================================================================================================
typedef struct{
   const char* name;
} SeqMgr_BodySeqKill;

///=================================================================================================
/// \struct SeqMgr_BodyMsgStatus
///=================================================================================================
typedef struct{
   const char* name;
   size_t timestamp;
   size_t seqidx;
} SeqMgr_BodyMsgStatus;

///=================================================================================================
/// \struct SeqMgr_BodyMsgSend
///=================================================================================================
#define SeqMgr_BodyMsgSignal Cquence_Sig

///=================================================================================================
/// \union SeqMgr_MsgBody
/// \brief Message body that can take a form of any defined message type
///=================================================================================================
typedef union{
  SeqMgr_BodySeqNew seq_new;
  SeqMgr_BodySeqPause seq_pause;
  SeqMgr_BodySeqKill seq_kill;
  SeqMgr_BodyMsgStatus msg_status;
  SeqMgr_BodyMsgSignal msg_signal;
} SeqMgr_MsgBody;

///=================================================================================================
/// \enum SeqMgr_MsgType
/// \brief Type of sequence message
///
/// Information used to determine, what type of message is sent to SeqMgr message queue
///=================================================================================================
typedef enum{
   SEQ_NEW,
   SEQ_PAUSE,
   SEQ_KILL,
   MSG_STATUS,
   MSG_SIGNAL
} SeqMgr_MsgType;

///=================================================================================================
/// \struct SeqMgr_Message
/// \brief Message structure used by SeqMgr
///
/// Both arguments are used inside operation implementation
///=================================================================================================
typedef struct{
  SeqMgr_MsgType msg_type;
  SeqMgr_MsgBody msg_body;
} SeqMgr_Message;

