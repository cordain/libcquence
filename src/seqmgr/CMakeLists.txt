project("libcquence_seqmgr_unit_test" C) 

cmake_minimum_required(VERSION 3.5)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/target)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -O0 -g -Wno-implicit-function-declaration")

set(FREERTOS_MEMMANG heap_3)
set(FREERTOS_PORT ThirdParty/GCC/Posix)

add_subdirectory($ENV{HOME}/.dev/tools/FreeRTOS-Kernel ${CMAKE_CURRENT_SOURCE_DIR}/FreeRTOS-Kernel)

set(TARGET "seqmgr_utest")

add_definitions(-DUNIT_TESTS)
add_link_options(-Wl,--wrap=RtosWrp_DeleteQueue,--wrap=RtosWrp_EngineQueue,--wrap=RtosWrp_ThreadRequest,--wrap=CquenceCfg_OpNameOfIndex,--wrap=RtosWrp_TaskTicks,--wrap=FreeRTOS_printf,--wrap=RtosWrp_QueuePush,--wrap=RtosWrp_QueuePop,--wrap=HandleSeqNew,--wrap=HandleSeqPause,--wrap=HandleSeqKill,--wrap=HandleMsgStatus,--wrap=HandleMsgSignal)
add_executable(
   ${TARGET}
   utest.c
)

target_include_directories(
   ${TARGET}
   PRIVATE
   .
   ../
   ${CMAKE_CURRENT_SOURCE_DIR}
   ../../tests/libcquence_usr
   ${FREERTOS_INC}
   ../../tests
)

target_link_libraries(${TARGET} PUBLIC pthread rt)
