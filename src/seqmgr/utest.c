#include "cester.h"

#ifndef UTEST_ONCE
#define UTEST_ONCE
#include "seqmgr.c"
#include <stdarg.h>
static unsigned char handler_called = 0xFF;
#endif

CESTER_MOCK_FUNCTION(RtosWrp_EngineQueue(),RtosWrp_QueueType,
   static RtosWrp_QueueType engine_queue_handles = 0;
   return ++engine_queue_handles;
)

CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_ThreadRequest(RtosWrp_EngineParams* params),void,)

CESTER_MOCK_FUNCTION(CquenceCfg_OpNameOfIndex(Cquence_OpCode op),const char *,
   static const char* op1 = "OP1";
   if(op >= USER_OP_COUNT)
      return NULL;
   else
      return &op1;
)

CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_TaskTicks(),RtosWrp_TimeType,0)

CESTER_MOCK_FUNCTION(FreeRTOS_printf(const char *pcFormat, ...),void,
   va_list args;
   va_start(args,pcFormat);

   unsigned ticks = va_arg(args, unsigned);
   const char * op_name = va_arg(args, const char *);
   const char * name = va_arg(args, const char *);
   unsigned seqidx = va_arg(args, unsigned);

   const char c = op_name[0];
   const char c2 = name[0];

   va_end(args);
)

CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_QueuePush(RtosWrp_QueueType queue, const void* buffer, RtosWrp_TimeType delay),bool,false)
CESTER_MOCK_SIMPLE_FUNCTION(RtosWrp_DeleteQueue(RtosWrp_QueueType queue),void,)

CESTER_MOCK_FUNCTION(RtosWrp_QueuePop(RtosWrp_QueueType queue, void* data, RtosWrp_TimeType delay),void*,
   switch(queue){
      case 0:
         data = NULL;
         return NULL;
      case 1:
         ((SeqMgr_Message*)data)->msg_type = SEQ_NEW;
         return data;
      case 2:
         ((SeqMgr_Message*)data)->msg_type = SEQ_PAUSE;
         return data;
      case 3:
         ((SeqMgr_Message*)data)->msg_type = SEQ_KILL;
         return data;
      case 4:
         ((SeqMgr_Message*)data)->msg_type = MSG_STATUS;
         return data;
      case 5:
         ((SeqMgr_Message*)data)->msg_type = MSG_SIGNAL;
         return data;
      default:
         CQUENCE_ASSERT(0 && "BAD TEST");
   }
)


CESTER_MOCK_FUNCTION(HandleSeqNew(SeqMgr_BodySeqNew* seq_new,RtosWrp_QueueType req_queue,RtosWrp_EngineParams* engines_params,uint8_t* cursor),void,
   handler_called = 8;
)
CESTER_MOCK_FUNCTION(HandleSeqPause(SeqMgr_BodySeqPause* seq_pause),void,
   handler_called = 9;
)
CESTER_MOCK_FUNCTION(HandleSeqKill(SeqMgr_BodySeqKill* seq_kill),void,
   handler_called = 10;
)
CESTER_MOCK_FUNCTION(HandleMsgStatus(SeqMgr_BodyMsgStatus* msg_status, RtosWrp_EngineParams* engines_params),void,
   handler_called = 11;
)
CESTER_MOCK_FUNCTION(HandleMsgSignal(
      SeqMgr_BodyMsgSignal* msg_signal,
      RtosWrp_EngineParams* engines_params
),void,
   handler_called = 12;
)

CESTER_TEST(sequence_ABC_on_second_place_in_engines_params_array, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   engines_params[1].name = "ABC";
   uint8_t cursor = GetCursorOfTaskName("ABC", engines_params);
   cester_assert_equal(cursor,1);
})
CESTER_TEST(sequence_CBA_on_last_place_in_engines_params_array, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   engines_params[SEQ_MAX_COUNT-1].name = "CBA";
   uint8_t cursor = GetCursorOfTaskName("CBA", engines_params);
   cester_assert_equal(cursor,SEQ_MAX_COUNT-1);
})
CESTER_TEST(sequence_CBA_not_found_in_engines_params_array, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   uint8_t cursor = GetCursorOfTaskName("CBA", engines_params);
   cester_assert_equal(cursor,SEQ_MAX_COUNT);
})

CESTER_TEST(add_sequence_AAA_to_empty_engines_params_array, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   uint8_t cursor = 0;
   RtosWrp_QueueType req_queue = 0xFF;
   Cquence_OpRequest seq_aaa[1] = {END_OF_SEQUENCE()};
   SeqMgr_BodySeqNew seq_new = {
      .name = "AAA",
      .sequence = seq_aaa
   };
   __real_HandleSeqNew(&seq_new, req_queue, engines_params, &cursor);
   cester_assert_equal(cursor,0);
   cester_assert_str_equal(engines_params[cursor].name,"AAA");
   cester_assert_ptr_equal(engines_params[cursor].sequence,seq_aaa);
   cester_assert_equal(engines_params[cursor].req_queue,0xFF);
   cester_assert_equal(engines_params[cursor].my_queue,1);
})
CESTER_TEST(add_sequence_AAA_to_full_engines_params_array_check_cursor, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   for(int i = 0; i < SEQ_MAX_COUNT; i++){
      engines_params[i].name = "";
   }
   uint8_t cursor = 0, entry_cursor = 0;
   RtosWrp_QueueType req_queue = 0xFF;
   Cquence_OpRequest seq_aaa[1] = {END_OF_SEQUENCE()};
   SeqMgr_BodySeqNew seq_new = {
      .name = "AAA",
      .sequence = seq_aaa
   };
   __real_HandleSeqNew(&seq_new, req_queue, engines_params, &cursor);
   cester_assert_equal(entry_cursor,cursor);
})
CESTER_TEST(add_sequence_AAA_to_full_engines_params_array_check_sequence_names_in_array, inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   for(int i = 0; i < SEQ_MAX_COUNT; i++){
      engines_params[i].name = "";
   }
   uint8_t cursor = 0, entry_cursor = 0;
   RtosWrp_QueueType req_queue = 0xFF;
   Cquence_OpRequest seq_aaa[1] = {END_OF_SEQUENCE()};
   SeqMgr_BodySeqNew seq_new = {
      .name = "AAA",
      .sequence = seq_aaa
   };
   __real_HandleSeqNew(&seq_new, req_queue, engines_params, &cursor);
   for(int i = 0; i < SEQ_MAX_COUNT; i++){
      cester_assert_str_equal(engines_params[i].name,"");
   }
})

CESTER_TEST(status_of_sequence_AAA_on_op1,inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   Cquence_OpRequest seq_aaa[2] = {
      CALL(OP1),
      END_OF_SEQUENCE()
   };
   engines_params[1].name = "AAA";
   engines_params[1].sequence = seq_aaa;
   SeqMgr_BodyMsgStatus status = {
      .name = "AAA",
      .seqidx = 0,
   };
   __real_HandleMsgStatus(&status,engines_params);
})
CESTER_TEST(status_of_sequence_AAA_on_undefined_operation,inst,{
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0};
   Cquence_OpRequest seq_aaa[2] = {
      CALL(USER_OP_COUNT),
      END_OF_SEQUENCE()
   };
   engines_params[1].name = "AAA";
   engines_params[1].sequence = seq_aaa;
   SeqMgr_BodyMsgStatus status = {
      .name = "AAA",
      .seqidx = 0,
   };
   __real_HandleMsgStatus(&status,engines_params);
})

CESTER_TEST(sequence_runtime_with_empty_queue, inst,{
   SeqMgr_Runtime(0);
   cester_assert_equal(handler_called,0xFF);
})
CESTER_TEST(sequence_runtime_with_new_sequence_command_in_queue, inst,{
   SeqMgr_Runtime(1);
   cester_assert_equal(handler_called,8);
})
CESTER_TEST(sequence_runtime_with_pause_sequence_command_in_queue, inst,{
   SeqMgr_Runtime(2);
   cester_assert_equal(handler_called,9);
})
CESTER_TEST(sequence_runtime_with_kill_sequence_command_in_queue, inst,{
   SeqMgr_Runtime(3);
   cester_assert_equal(handler_called,10);
})
CESTER_TEST(sequence_runtime_with_sequence_status_in_queue, inst,{
   SeqMgr_Runtime(4);
   cester_assert_equal(handler_called,11);
})
CESTER_TEST(sequence_runtime_with_sequence_message_in_queue, inst,{
   SeqMgr_Runtime(5);
   cester_assert_equal(handler_called,12);
})

CESTER_TODO_TEST(test_HandleSeqPause_after_implementation,inst,

)
CESTER_TODO_TEST(test_HandleSeqKill_after_implementation,inst,

)

CESTER_BEFORE_EACH(test_instance, test_name, index,
   handler_called = 0xFF;
)

CESTER_OPTIONS(
   CESTER_TEST_SHOULD_FAIL(status_of_sequence_AAA_on_undefined_operation);
)

