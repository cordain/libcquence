///=================================================================================================
/// \file seqmgr.c
///
/// This file contains implementation of seqmgr module
///=================================================================================================
#include "seqmgr.h"
#include "seqmgr_message.h"
#include "cquence_types.h"
#include <string.h>
#include "rtoswrp/rtoswrp.h"
#include "engine/engine.h"
#include "config/config_if.h"
#include "config.h"

#include "cquence_internal.h"

#define ENGINES_PARAMS_NEXT(x) ++(x)%SEQ_MAX_COUNT

static uint8_t GetCursorOfTaskName(const char* name, RtosWrp_EngineParams engines_params[]){
   uint8_t cursor;
   for(cursor = 0; cursor < SEQ_MAX_COUNT; cursor++){
      if(engines_params[cursor].name != NULL && !strcmp(engines_params[cursor].name,name))
         break;
   }
   CQUENCE_ASSERT(cursor < SEQ_MAX_COUNT);
   return cursor;
}

CQUENCE_PRIVATE_FN(void,HandleSeqNew)(
      SeqMgr_BodySeqNew* seq_new,
      RtosWrp_QueueType req_queue,
      RtosWrp_EngineParams engines_params[],
      uint8_t* cursor
){
   uint8_t entry_cursor = *cursor;
   bool first_pass = true;
   do{
      if(engines_params[*cursor].name == NULL){
         engines_params[*cursor].name = seq_new->name;
         engines_params[*cursor].sequence = seq_new->sequence;
         engines_params[*cursor].req_queue = req_queue;
         engines_params[*cursor].my_queue = RtosWrp_EngineQueue(seq_new->queue_size);
         break;
      }
      first_pass = false;
      *cursor = ENGINES_PARAMS_NEXT(*cursor);
   } while(*cursor != entry_cursor);
   CQUENCE_ASSERT(first_pass || *cursor != entry_cursor);
   RtosWrp_ThreadRequest(&engines_params[*cursor]);
}

CQUENCE_PRIVATE_FN(void,HandleSeqPause)(SeqMgr_BodySeqPause* seq_pause){
   //TODO
}

CQUENCE_PRIVATE_FN(void,HandleSeqKill)(SeqMgr_BodySeqKill* seq_kill){
   //TODO
}

CQUENCE_PRIVATE_FN(void,HandleMsgStatus)(SeqMgr_BodyMsgStatus* msg_status, RtosWrp_EngineParams* engines_params){
   uint8_t cursor = GetCursorOfTaskName(msg_status->name, engines_params);
   const Cquence_OpRequest* current_request = &engines_params[cursor].sequence[msg_status->seqidx];
   const char* op_name;
   switch(current_request->op_request_type){
      case OP_REQUEST_CALL:
         op_name = CquenceCfg_OpNameOfIndex(
            current_request->op_request_body.op_request_call.operation
         );
      break;
   case OP_REQUEST_SEND:
      op_name = "SEND";
      break;
   case OP_REQUEST_RECEIVE:
      op_name = "RECEIVE";
      break;
   case OP_REQUEST_BRANCH:
      op_name = "BRANCH";
      break;
   case OP_REQUEST_END_OF_SEQUENCE:
      op_name = "EOS";
      RtosWrp_DeleteQueue(engines_params[cursor].my_queue);
      engines_params[cursor].name = NULL;
      break;
   }
   CQUENCE_ASSERT(op_name != NULL);
   FreeRTOS_printf("%10d: %s as %s[%zu] is done\n",
      msg_status->timestamp,
      op_name,
      msg_status->name,
      msg_status->seqidx
   );
}

CQUENCE_PRIVATE_FN(void,HandleMsgSignal)(
      SeqMgr_BodyMsgSignal* msg_signal,
      RtosWrp_EngineParams* engines_params
){
   uint8_t cursor = GetCursorOfTaskName(msg_signal->recipent,engines_params);
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(engines_params[cursor].my_queue, (void*)msg_signal, 100));
}

void SeqMgr_Runtime(RtosWrp_QueueType req_queue){
   RtosWrp_EngineParams engines_params[SEQ_MAX_COUNT] = {0}; 
   uint8_t engines_params_cursor = 0;

   CQUENCE_FOREVER{
      //PopReqQueue()
      SeqMgr_Message current_msg;
      if(RtosWrp_QueuePop(req_queue, (void*)&current_msg, RTOSWRP_INFINITE_DELAY) != NULL){
         switch (current_msg.msg_type){
            case SEQ_NEW:
               HandleSeqNew(
                  &(current_msg.msg_body.seq_new),
                  req_queue,
                  engines_params,
                  &engines_params_cursor
               );
               break;
            case SEQ_PAUSE:
               HandleSeqPause(&(current_msg.msg_body.seq_pause));
               break;
            case SEQ_KILL:
               HandleSeqKill(&(current_msg.msg_body.seq_kill));
               break;
            case MSG_STATUS:
               HandleMsgStatus(&(current_msg.msg_body.msg_status),engines_params);
               break;
            case MSG_SIGNAL:
               HandleMsgSignal(&(current_msg.msg_body.msg_signal),engines_params);
               break;
            default:
               break;
         }
      }
   }
}
