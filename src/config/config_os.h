///=================================================================================================
/// \file config_os.h
///
/// This file defines supported OS with static choise from user configuration
///=================================================================================================

#ifndef LIBCQUENCE_CONFIG_OS_H
#define LIBCQUENCE_CONFIG_OS_H

#include "config.h"

#if USE_OS_NO==0
//TODO this version will be present until work on freertos_posix will be started
#define USE_NO_OS
//#error <No os selected>
#elif USE_OS_NO==1
#define USE_FREERTOS_POSIX
#elif USE_OS_NO==255
#define USE_UNIT_TEST
#else
#error <Unsupported OS selected>
#endif
#endif//LIBCQUENCE_CONFIG_OS_H
