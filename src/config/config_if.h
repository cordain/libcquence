///=================================================================================================
/// \file config_if.h
///
// This file contains interfaces of config module
///=================================================================================================

#ifndef LIBCQUENCE_CFG_IF_H
#define LIBCQUENCE_CFG_IF_H

#include "cquence_types.h"

///================================================================================================
/// \def   Config_Cquence_OpMetaOfIndex
/// \brief Function that accesses configuration metadata of config.
///
/// This macro can be of any type, but requirement is to receive Cquence_OpMeta type.
///
/// \param op - operation code defined in Config_Cquence_OpCode
///
/// \return Cquence_OpMeta* pointer to metadata under index, null if not found
///================================================================================================
const Cquence_OpImpl CquenceCfg_Cquence_OpImplOfIndex(Cquence_OpCode op);
const char* CquenceCfg_OpNameOfIndex(Cquence_OpCode op);

#endif//LIBCQUENCE_CFG_IF_H
