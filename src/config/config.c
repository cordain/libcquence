#include "config_if.h"

///================================================================================================
/// \var  Config_OperationMetadata
/// \brief Configuration of metadata.
///
/// Here metadata is arranged with respect to enumerated Cquence_OpCodes (see Config_Cquence_OpCode)
///================================================================================================
extern const Cquence_OpImpl operation_calls[USER_OP_COUNT];

///================================================================================================
/// \var  Config_OperationMetadata
/// \brief Configuration of metadata.
///
/// Here metadata is arranged with respect to enumerated Cquence_OpCodes (see Config_Cquence_OpCode)
///================================================================================================
extern const char* operation_str[USER_OP_COUNT];

///================================================================================================
/// \fn CquenceCfg_Cquence_OpMetaOfIndex
/// \brief implementation of extern function prototype
///================================================================================================
const Cquence_OpImpl CquenceCfg_Cquence_OpImplOfIndex(Cquence_OpCode op){
   assert(op < USER_OP_COUNT);
   return operation_calls[op]; //Config_OperationAt
}

///================================================================================================
/// \fn CquenceCfg_OpNameOfIndex
/// \brief implementation of extern function prototype
///================================================================================================
const char* CquenceCfg_OpNameOfIndex(Cquence_OpCode op){
   assert(op < USER_OP_COUNT);
   return operation_str[op]; //Config_OperationAt
}
