# Guideline reason

This document is written for further development to assure that next software iteration will contain compliant code.

## Identifies
- Words in identifier shall be CamelCase
- Global definitions shall have prototypes defined in header files
- Module prefix is required only in global identifiers
- Module prefix shall start with capital letter
- Public API identifieers shall have library name as prefix
## Fields and aruments
- Definition fields and arguments shall follow variable naming convention

## Functions
### Arguments
- Function prototypes shall have argument names
