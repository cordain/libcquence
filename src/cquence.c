#include "cquence.h"
#include "seqmgr/seqmgr.h"
#include "seqmgr/seqmgr_message.h"
#include "rtoswrp/rtoswrp.h"
#include "cquence_types.h"
#include "config.h"

static RtosWrp_QueueType seqmgr_queue;

void Cquence_Init(){
   seqmgr_queue = RtosWrp_SeqmgrQueueInit(SEQMGR_QUEUE_SIZE);
   CQUENCE_ASSERT(seqmgr_queue);
   RtosWrp_SpawnSeqMgr(seqmgr_queue);
}

void Cquence_NewSequence(const char* name, const Cquence_OpRequest* seq, size_t queue_size){
   const SeqMgr_Message req_seq = {
      .msg_type = SEQ_NEW,
      .msg_body = (SeqMgr_MsgBody){
         .seq_new = (SeqMgr_BodySeqNew){
            .name = name,
            .sequence = seq,
            .queue_size = queue_size
         }
      }
   };
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(seqmgr_queue, (void*)&req_seq, 0));
}

void Cquence_PauseSequence(const char* seq_name, size_t ticks){
   const SeqMgr_Message pause_seq = {
      .msg_type = MSG_SIGNAL,
      .msg_body = (SeqMgr_MsgBody){
         .msg_signal = (SeqMgr_BodyMsgSignal){
            .recipent = seq_name,
            .sender = "API",
            .sig_type = SIG_PAUSE,
            .sig_body = (Cquence_SigBody){
               .sig_pause = (Cquence_BodySigPause){
                  .pause_for_ticks = ticks
               }
            }
         }
      }
   };
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(seqmgr_queue, (void*)&pause_seq, 0));
}

void Cquence_ResumeSequence(const char* seq_name, size_t delay){
   const SeqMgr_Message resume_seq = {
      .msg_type = MSG_SIGNAL,
      .msg_body = (SeqMgr_MsgBody){
         .msg_signal = (SeqMgr_BodyMsgSignal){
            .recipent = seq_name,
            .sender = "API",
            .sig_type = SIG_RESUME,
            .sig_body = (Cquence_SigBody){
               .sig_resume = (Cquence_BodySigResume){
                  .resume_in_ticks = delay
               }
            }
         }
      }
   };
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(seqmgr_queue, (void*)&resume_seq, 0));
}

void Cquence_KillSequence(const char* seq_name, size_t delay){
   const SeqMgr_Message kill_seq = {
      .msg_type = MSG_SIGNAL,
      .msg_body = (SeqMgr_MsgBody){
         .msg_signal = (SeqMgr_BodyMsgSignal){
            .recipent = seq_name,
            .sender = "API",
            .sig_type = SIG_KILL,
            .sig_body = (Cquence_SigBody){
               .sig_kill = (Cquence_BodySigKill){
                  .kill_in_ticks = delay
               }
            }
         }
      }
   };
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(seqmgr_queue, (void*)&kill_seq, 0));
}

void Cquence_ReplaceArgument(const char* seq_name, size_t index, void* argument, size_t argument_size){
   const SeqMgr_Message mod_seq = {
      .msg_type = MSG_SIGNAL,
      .msg_body = (SeqMgr_MsgBody){
         .msg_signal = (SeqMgr_BodyMsgSignal){
            .recipent = seq_name,
            .sender = "API",
            .sig_type = SIG_MOD_SEQ,
            .sig_body = (Cquence_SigBody){
               .sig_mod_seq = (Cquence_BodySigModSeq){
                  .seq_idx = index,
                  .data = argument,
                  .data_len = argument_size
               }
            }
         }
      }
   };
   CQUENCE_ASSERT(true == RtosWrp_QueuePush(seqmgr_queue, (void*)&mod_seq, 0));
}
