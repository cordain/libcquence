# eCquencer Library

This project aims to create a library for read and execute sequences. This piece of code is written in C for embedded devices, using FreeRTOS.

# Concept

Idea of the project is to bring the sequencing mechanism, so the user can smoothly and without breakes and pauses execute commands in own project.

For more details please refer to [architecture](arch.md#stack-overview)

## Config

### Command definition

Core part of configuration is the command definition. These data shall be constructed
before compiling the project.

```plantuml
@startuml

object OperationEnum{
 op1
 op2
 op3
 ...
}

object OperationMetas{
 op1meta: OpMeta
 op2meta: OpMeta
 op3meta: OpMeta
 ...
}

@enduml
```

Operation metadata is of following type:
```plantuml
@startuml

object OpMeta{
 implementation: fn(*void, *void)
 blocking: bool
}

@enduml
```

### Custom types definition

This part of configuration should contain custom parameter types for the operations.
As operation request has no strict type for parameters, this can be any type.

### Sequence definition

This part needs to be constructed in aplication, due to highly configurable nature of sequence.

```plantuml
@startuml

object Sequence{
 seqop[0]: ExecutionRequest
 seqop[1]: ExecutionRequest
 seqop[2]: ExecutionRequest
 ...
}

@enduml
```

## Sequence engine

Command is a defined operation, that has it's coded value and assigned inputs and executuion procedure.
To reach centrain command, Execution Request needs to be created.
After operation is done, Execution Result is provided to the caller, giving data that command produced or error code with explanation.

```plantuml
@startuml

object Result{
 ErrCode: Enum
 retval: void*
}

object ExecutionRequest{
 OpCode: Enum
 arg0: void*
 arg1: void*
}

object ExecutionResult{
 OpCode: Enum
 retval: Result
}

@enduml
```

To ensure that operations run smoothly, without breakes, requests are stored in a queue.

Below diagram shows execution of an engine thread.
```plantuml
@startuml

start
if (Is running a command) then (yes)
 repeat :Pop from result queue;
 backward:Pass result to the caller;
 repeat while (result queue empty) is (no)
endif
:Check operation queue;
if (Item in operation queue) then (yes)
 :Check command ID;
 if (Command ID in range) then (yes)
  :Execute command;
 endif
endif
stop

@enduml
```
### Sequences

Let's say we have coded two operations: "Operation 1" and "Operation 2".
Following sequences will demonstrate, how an engine shall work, when used directly.

