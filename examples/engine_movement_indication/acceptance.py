#!/usr/bin/python3

import re

execution_time = 40.01

# define user helper variables
__HEAD_MOVE_TIME_DELTA = 20000
__WAITFOR_CALLS_COUNT=3
__LED_COMMANDS_COUNT=6
__ENGINE_COMMANDS_COUNT=5
__NOTIFY_WAITFOR_PAIR_COUNT=3


def test_equal(test_name,left,right) -> bool:
    print('='*80)
    print(test_name)
    condition = left == right
    print(f"{left}=={right} is {condition}")
    return condition

def test_arr_equal_param(test_name,arr,param) -> bool:
    print('='*80)
    print(test_name)
    conditions = [item == param for item in arr]
    valid_count = conditions.count(True)
    print(f"{valid_count} out of {len(conditions)} elements are equal to {param}")
    return not (False in conditions)

def test_arr_equal(test_name,left,right) -> bool:
    print('='*80)
    print(test_name)
    if len(left) != len(right):
        print(f"left({len(left)}) != right({len(right)})")
        return False
    lrzip = zip(left, right)
    conditions = [litem == ritem for litem, ritem in lrzip]
    lrczip = zip(left,right,conditions)
    print("\n".join([f"{litem}=={ritem} = {condition}" for litem,ritem,condition in lrczip]))
    return not (False in conditions)

def execute(output_trace:str) -> bool:
    result = []

    head_move_times_re = re.compile(r"([ \d]+): HEAD_MOVE as \w+\[\d+\] \w{2,3} \w{4,5}",re.M)
    send_from_engine_times_re = re.compile(r"([ \d]+): SEND as engine\[\d+\] \w{2,3} \w{4,5}",re.M)
    receive_from_engine_times_re = re.compile(r"([ \d]+): RECEIVE as led\[\d+\] \w{2,3} \w{4,5}",re.M)
    branch_waiting_for_engine_times_re = re.compile(r"([ \d]+): BRANCH as led\[\d+\] \w{2,3} \w{4,5}",re.M)
    led_level_times_re = re.compile(r"([ \d]+): LED_LEVEL as led\[\d+\] \w{2,3} \w{4,5}",re.M)
    
    timestamps = [int(match.lstrip()) for match in head_move_times_re.findall(output_trace)]
    head_move_deltas = [timestamps[i+1] - timestamps[i] for i in range(len(timestamps)-1)]
    send_from_engine_timestamps = [int(match.lstrip()) for match in send_from_engine_times_re.findall(output_trace)]
    receive_from_engine_timestapms = [int(match.lstrip()) for match in receive_from_engine_times_re.findall(output_trace)]
    timestamps = [int(match.lstrip()) for match in branch_waiting_for_engine_times_re.findall(output_trace)]
    receive_from_engine_timestapms.append(timestamps[-1])
    branches_led_blinking_times = [timestamps[i+1] - timestamps[i] for i in range(len(timestamps)-1)][:-1]
    led_level_timestamps = [int(match.lstrip()) for match in led_level_times_re.findall(output_trace)][:-1]

    result.append(test_arr_equal_param("check timings of head move",head_move_deltas,20000))
    result.append(test_arr_equal("check timings of send from engine",send_from_engine_timestamps,[0,20000,40000]))
    result.append(test_arr_equal("check timestamps of receive from engine",receive_from_engine_timestapms,[0,20000,40000]))
    result.append(test_arr_equal_param("check delay between blinks",branches_led_blinking_times,1000))
    result.append(test_arr_equal("check timestamps of led level",led_level_timestamps,[ 0, 20000, 21000, 22000, 23000, 24000, 25000, 26000, 27000, 28000, 29000, 30000, 31000, 32000, 33000, 34000, 35000, 36000, 37000, 38000, 39000, 40000]))
    return not (False in result)
