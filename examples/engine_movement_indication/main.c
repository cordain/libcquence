#include "cquence.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "sequences.c"

int main(){
   Cquence_Init();
   Cquence_NewSequence(LED_NAME,led,1);
   Cquence_NewSequence(ENGINE_NAME,engine,1);
   printf("Sequences added\n");
   vTaskStartScheduler();
}

void vAssertCalled(const char * const pcFileName, unsigned long ulLine)
{
        taskENTER_CRITICAL();

        fprintf(stderr, "ASSERT: %s:%lu\n", pcFileName, ulLine);
        exit(-1);

        taskEXIT_CRITICAL();
}

void vLoggingPrintf(const char *pcFormat, ...)
{
        taskENTER_CRITICAL();

        va_list arg;

        va_start( arg, pcFormat );
        vprintf( pcFormat, arg );
        va_end( arg );

        taskEXIT_CRITICAL();
}

