#include "cquence_types.h"
#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"


///=================================================================================================
/// USER OP IMPLEMENTATION
    
Cquence_OpResult Cquence_HeadMoveOperation(void* arg){
   const float speed = 5.f/100.f;
   bool successful = false;
   Cquence_OpCode_HEAD_MOVE_Arg* data = arg;
   FreeRTOS_printf("Start moving header by %d\n\n",data->delta);
   successful = true;
   return (Cquence_OpResult){.successful=successful};
}

Cquence_OpResult Cquence_LedLevelOperation(void* arg){
   Cquence_OpCode_LED_LEVEL_Arg* data = arg;
   FreeRTOS_printf("Led at level %zu\n",data->level);
   return (Cquence_OpResult){.successful=true};
}

///=================================================================================================
