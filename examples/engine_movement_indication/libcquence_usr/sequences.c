#include "cquence_types.h"
#include "opcodes.h"
#define LED_NAME "led"
#define ENGINE_NAME "engine"

const Cquence_OpRequest led[] = {
    RECEIVE(ENGINE_NAME),
    CALL(LED_LEVEL,
        .level=100U,
    ),
    RECEIVE(ENGINE_NAME),
    CALL(LED_LEVEL,
        .level=0U,
    ),
    BRANCH(true,ENGINE_NAME,1000U,7),
    CALL(LED_LEVEL,
        .level=100U,
    ),
    BRANCH(false,ENGINE_NAME,1000U,3),
    CALL(LED_LEVEL,
        .level=0U,
    ),
    END_OF_SEQUENCE()
};

const Cquence_OpRequest engine[] = {
    SEND(LED_NAME),
    CALL(HEAD_MOVE,
        .delta=1000,
    ),
    BRANCH(false,"",20000U,3),
    SEND(LED_NAME),
    CALL(HEAD_MOVE,
        .delta=-1000,
    ),
    BRANCH(false,"",20000U,6),
    SEND(LED_NAME),
    END_OF_SEQUENCE()
};
