///=================================================================================================
/// \\file opcodes.h
///
/// This file contains cofiguration that describes used operations
/// Please respect sections that covers DO NOT CHANGE code
///=================================================================================================

#ifndef LIBCQUENCE_OPCODES_H
#define LIBCQUENCE_OPCODES_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
typedef struct{
   int32_t delta;
} Cquence_OpCode_HEAD_MOVE_Arg;

typedef struct{
   uint8_t level;
} Cquence_OpCode_LED_LEVEL_Arg;


///================================================================================================
/// \\enum  Config_OpCode
/// \\brief Identifiers of operation codes.
///
/// Defined opcodes need to be started from 0 and be left with no empty values,
/// otherwise library will not be working
///================================================================================================
typedef enum{
///=================================================================================================
/// USER OPCODES
   HEAD_MOVE,
   LED_LEVEL,
///=================================================================================================

///=================================================================================================
/// DO NOT CHANGE(len,eos) - standard values used in library
   USER_OP_COUNT,
///=================================================================================================
}Cquence_OpCode;
#endif//LIBCQUENCE_OPCODES_H