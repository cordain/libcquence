
///=================================================================================================
/// \\file usr.c
///
/// This file contains static data of configuration, including opmeta storage and external definitions of operation functions
///=================================================================================================

#include "config.h"
#include "cquence_types.h"

///================================================================================================
/// \\typedef EXTERNOP
/// \\brief Macro helper for creating function prototypes
///================================================================================================
#define EXTERNOP(name) extern Cquence_OpResult Cquence_ ## name ## Operation(void*)

///================================================================================================
/// \\typedef OPMETA
/// \\brief Macro helper for creating Cquence_OpMeta instance in operation_metadata array
///================================================================================================
#define OPCALL(opimpl) Cquence_ ## opimpl ## Operation

///================================================================================================
/// USER OP IMPLEMENTATION PROTOTYPES
EXTERNOP(HeadMove);
EXTERNOP(LedLevel);
///================================================================================================

const Cquence_OpImpl operation_calls[USER_OP_COUNT] = {
///================================================================================================
/// USER OP META LIST
   OPCALL(HeadMove),
   OPCALL(LedLevel),
///================================================================================================
};

const char* operation_str[USER_OP_COUNT] = {
///================================================================================================
/// USER OP META LIST
"HEAD_MOVE",
"LED_LEVEL",
///================================================================================================
};
