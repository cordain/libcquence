///=================================================================================================
/// \\file opcodes.h
///
/// This file contains cofiguration that describes used operations
/// Please respect sections that covers DO NOT CHANGE code
///=================================================================================================

#ifndef LIBCQUENCE_OPCODES_H
#define LIBCQUENCE_OPCODES_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
typedef struct{
   uint16_t amp;
   size_t delay;
   uint8_t channel;
} Cquence_OpCode_AMPLITUDE_LINEAR_Arg;

typedef struct{
   uint16_t amp;
   uint8_t channel;
} Cquence_OpCode_AMPLITUDE_SET_Arg;


///================================================================================================
/// \\enum  Config_OpCode
/// \\brief Identifiers of operation codes.
///
/// Defined opcodes need to be started from 0 and be left with no empty values,
/// otherwise library will not be working
///================================================================================================
typedef enum{
///=================================================================================================
/// USER OPCODES
   AMPLITUDE_LINEAR,
   AMPLITUDE_SET,
///=================================================================================================

///=================================================================================================
/// DO NOT CHANGE(len,eos) - standard values used in library
   USER_OP_COUNT,
///=================================================================================================
}Cquence_OpCode;
#endif//LIBCQUENCE_OPCODES_H