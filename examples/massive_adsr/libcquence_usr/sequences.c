#include "cquence_types.h"
#include "opcodes.h"
#define ADS_NAME "ads"
#define REL_NAME "rel"

const Cquence_OpRequest ads[] = {
    CALL(AMPLITUDE_LINEAR,
        .amp=100U,
        .delay=100U,
        .channel=0U,
    ),
    BRANCH(true,REL_NAME,100U,6),
    CALL(AMPLITUDE_LINEAR,
        .amp=80U,
        .delay=1000U,
        .channel=0U,
    ),
    BRANCH(true,REL_NAME,1000U,6),
    CALL(AMPLITUDE_SET,
        .amp=80U,
        .channel=0U,
    ),
    RECEIVE(REL_NAME),
    END_OF_SEQUENCE(),
};

const Cquence_OpRequest rel[] = {
    SEND(ADS_NAME),
    CALL(AMPLITUDE_LINEAR,
        .amp=0U,
        .delay=2000U,
        .channel=0U,
    ),
    BRANCH(false,"",2000U,3),
    END_OF_SEQUENCE()
};
