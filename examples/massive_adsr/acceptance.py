#!/usr/bin/python3

import re

execution_time = 17

# define user helper variables
_channel_count = 20
_channel_adsr_times = [
   [ 100,   50,    0 ],
   [ 200,  100,  100 ],
   [ 300,   70,  200 ],
   [ 400,   30,  300 ],
   [ 500,    0,  400 ],
   [ 600,   30,  500 ],
   [ 700,   70,  600 ],
   [ 800,  100,  700 ],
   [ 900,   70,  800 ],
   [1000,   30,  900 ],
   [1100,    0, 1000 ],
   [1200,   30, 1100 ],
   [1300,   70, 1200 ],
   [1400,  100, 1300 ],
   [1500,   50, 1400 ],
   [   0,    0, 1500 ],
   [1500,   50, 1400 ],
   [1400,   70, 1400 ],
   [1300,  100, 1400 ],
   [1200, 2000, 1400 ]
]
_channel_sustain = 100
_subtest_times = [0, 2000, 4000, 8000, 12000, 17000]
_subtest_names = [
    "first burst", 
    "second burst", 
    "attack just before decay",
    "attack to decay",
    "attack, decay, sustain"
]
def _expected_timestamps_in_first_burst_for_channel(channel, time_offset_index):
    return [
        channel+_subtest_times[time_offset_index], 
        _channel_count+channel+_subtest_times[time_offset_index], 
        _channel_count+channel+_subtest_times[time_offset_index]
    ]
def _expected_timestamps_in_second_burst_for_channel(channel, time_offset_index):
    return [
        channel+_subtest_times[time_offset_index], 
        _channel_count+channel+_subtest_times[time_offset_index], 
        _channel_count+channel+_subtest_times[time_offset_index]
    ]
def _expected_timestamps_in_attack_just_before_decay_for_channel(channel, time_offset_index):
    return [
        channel+_subtest_times[time_offset_index], 
        _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index]-1, 
        _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index]-1
    ]
def _expected_timestamps_in_attack_to_decay_for_channel(channel, time_offset_index):
    return [
            channel+_subtest_times[time_offset_index], 
            _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index], 
            _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index], 
            _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index], 
            _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index]
        ]
def _expected_timestamps_in_attack_decay_sustain_for_channel(channel, time_offset_index):
    return [
        channel+_subtest_times[time_offset_index], 
        _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index], 
        _channel_adsr_times[channel][0]+channel+_subtest_times[time_offset_index], 
        _channel_adsr_times[channel][0]+_channel_adsr_times[channel][1]+channel+_subtest_times[time_offset_index], 
        _channel_adsr_times[channel][0]+_channel_adsr_times[channel][1]+channel+_subtest_times[time_offset_index],
        _channel_adsr_times[channel][0]+_channel_adsr_times[channel][1]+channel+_subtest_times[time_offset_index]+_channel_sustain,
        _channel_adsr_times[channel][0]+_channel_adsr_times[channel][1]+channel+_subtest_times[time_offset_index]+_channel_sustain,
    ]
_expected_subtest_timestamps = [
    [
        _expected_timestamps_in_first_burst_for_channel(i,0) for i in range(_channel_count)
    ],
    [
        _expected_timestamps_in_second_burst_for_channel(i,1) for i in range(_channel_count)
    ],
    [
        _expected_timestamps_in_attack_just_before_decay_for_channel(i,2) for i in range(_channel_count)
    ],
    [
        _expected_timestamps_in_attack_to_decay_for_channel(i,3) for i in range(_channel_count)
    ],
    [
        _expected_timestamps_in_attack_decay_sustain_for_channel(i,4) for i in range(_channel_count)
    ]
]

def test_equal(test_name,left,right) -> bool:
    print('='*80)
    print(test_name)
    condition = left == right
    print(f"{left}=={right} is {condition}")
    return condition

def test_arr_equal_param(test_name,arr,param) -> bool:
    print('='*80)
    print(test_name)
    conditions = [item == param for item in arr]
    valid_count = conditions.count(True)
    print(f"{valid_count} out of {len(conditions)} elements are equal to {param}")
    return not (False in conditions)

def test_arr_equal(test_name,left,right) -> bool:
    print('='*80)
    print(test_name)
    if len(left) != len(right):
        print(f"left({len(left)}) != right({len(right)})")
        return False
    lrzip = zip(left, right)
    conditions = [litem == ritem for litem, ritem in lrzip]
    lrczip = zip(left,right,conditions)
    print("\n".join([f"{litem}=={ritem} = {condition}" for litem,ritem,condition in lrczip]))
    return not (False in conditions)

_expected_subtest_timestamps[3][4] = [
        4+_subtest_times[3], 
        _channel_adsr_times[4][0]+4+_subtest_times[3], 
        _channel_adsr_times[4][0]+4+_subtest_times[3], 
        _channel_adsr_times[4][0]+_channel_adsr_times[4][1]+4+_subtest_times[3], 
        _channel_adsr_times[4][0]+_channel_adsr_times[4][1]+4+_subtest_times[3],
        _channel_adsr_times[4][0]+_channel_adsr_times[4][1]+4+_subtest_times[3],
        _channel_adsr_times[4][0]+_channel_adsr_times[4][1]+4+_subtest_times[3],
    ]
_expected_subtest_timestamps[3][10] = [
        10+_subtest_times[3], 
        _channel_adsr_times[10][0]+10+_subtest_times[3], 
        _channel_adsr_times[10][0]+10+_subtest_times[3], 
        _channel_adsr_times[10][0]+_channel_adsr_times[10][1]+10+_subtest_times[3], 
        _channel_adsr_times[10][0]+_channel_adsr_times[10][1]+10+_subtest_times[3],
        _channel_adsr_times[10][0]+_channel_adsr_times[10][1]+10+_subtest_times[3],
        _channel_adsr_times[10][0]+_channel_adsr_times[10][1]+10+_subtest_times[3],
    ]

_expected_subtest_timestamps[0][15] = [
        15+_subtest_times[0], 
        15+_subtest_times[0], 
        15+_subtest_times[0], 
        15+_subtest_times[0], 
        15+_subtest_times[0], 
        _channel_count+15+_subtest_times[0], 
        _channel_count+15+_subtest_times[0]
    ]
_expected_subtest_timestamps[1][15] = [
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        _channel_count+15+_subtest_times[1], 
        _channel_count+15+_subtest_times[1]
    ]
_expected_subtest_timestamps[1][15] = [
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        15+_subtest_times[1], 
        _channel_count+15+_subtest_times[1], 
        _channel_count+15+_subtest_times[1]
    ]
_expected_subtest_timestamps[2][15] = [
        15+_subtest_times[2], 
        15+_subtest_times[2], 
        15+_subtest_times[2], 
        15+_subtest_times[2], 
        15+_subtest_times[2], 
        15+_subtest_times[2], 
        15+_subtest_times[2], 
    ]
_expected_subtest_timestamps[3][15] = [
        15+_subtest_times[3], 
        _channel_adsr_times[15][0]+15+_subtest_times[3], 
        _channel_adsr_times[15][0]+15+_subtest_times[3], 
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[3], 
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[3],
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[3],
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[3],
    ]
_expected_subtest_timestamps[4][15] = [
        15+_subtest_times[4], 
        _channel_adsr_times[15][0]+15+_subtest_times[4], 
        _channel_adsr_times[15][0]+15+_subtest_times[4], 
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[4], 
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[4],
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[4]+_channel_sustain,
        _channel_adsr_times[15][0]+_channel_adsr_times[15][1]+15+_subtest_times[4]+_channel_sustain,
    ]

def execute(output_trace:str) -> bool:
    result = []
    
    for i in range(_channel_count):
        # get timestamps of i'th ads sequence log
        all_timestamps = [int(match.lstrip()) for match in re.findall(f"([ \\d]+): \\w+ as ads_{i}\\[\\d+\\] \\w{{2,3}} \\w{{4,5}}",output_trace)]
        # group timestamps to subtest chunks
        subtest_timestamps = [[] for _ in range(len(_subtest_times)-1)]
        subtest_idx = 0
        for timestamp in all_timestamps:
            if timestamp >= _subtest_times[subtest_idx+1]:
                subtest_idx+=1
            subtest_timestamps[subtest_idx].append(timestamp)
#        # test timestamp counts for subtests
#        for j in range(len(_subtest_names)):
#            result.append(test_equal(f"Check count of ads_{i} commands in {_subtest_names[j]} test",len(subtest_timestamps[j]),_expected_command_counts[i][j]))
#        if False in result:
#            break
        # test expected timestamps
        for j in range(len(_subtest_names)):
            result.append(test_arr_equal(f"Check timestamps of ads_{i} commands in {_subtest_names[j]} test", subtest_timestamps[j],_expected_subtest_timestamps[j][i]))

    return not False in result
