//Event Keyboard reader
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <fcntl.h>
#include <linux/input.h>

#define EVKBD_COUNT 3

typedef struct{
   char key;
   bool pressed;
}KeyboardEvent;

#ifndef ACCEPTANCE_TESTS
static int event_files[EVKBD_COUNT] = {0};
struct termios saved_attributes;

void evkbd_deinit()
{
   tcsetattr (STDIN_FILENO, TCSANOW, &saved_attributes);
}

void evkbd_init()
{
   struct termios tattr;

   char *name;
   DIR *dp;
   struct dirent *ep;

   dp = opendir ("/dev/input/by-id");
   if (!dp)
   {
      fprintf (stderr, "Couldn't open the directory\n");
      exit (EXIT_FAILURE);
   }
   for(size_t i = 0; i < EVKBD_COUNT;){
      if (!(ep = readdir (dp))) break;
      if (strstr(ep->d_name,"kbd")){
         char input_base[100] = "/dev/input/by-id/";
         if((event_files[i] = open(strcat(input_base,ep->d_name), O_RDONLY)) < 0) fprintf(stderr, "%s was not able to be read from. Probably root access needed.",ep->d_name); 
         i++;
      }
   }
   (void) closedir (dp);

   /* Make sure stdin is a terminal. */
   if (!isatty (STDIN_FILENO))
   {
      fprintf (stderr, "Not a terminal.\n");
      exit (EXIT_FAILURE);
   }

   /* Save the terminal attributes so we can restore them later. */
   tcgetattr (STDIN_FILENO, &saved_attributes);
   atexit (evkbd_deinit);

   /* Set the funny terminal modes. */
   tcgetattr (STDIN_FILENO, &tattr);
   tattr.c_lflag &= ~(ICANON|ECHO); /* Clear ICANON and ECHO. */
   tattr.c_cc[VMIN] = 1;
   tattr.c_cc[VTIME] = 0;
   tcsetattr (STDIN_FILENO, TCSAFLUSH, &tattr);
}
bool evkbd_poll(KeyboardEvent* event){
   struct input_event ev = {0};
   for(size_t i = 0; i < EVKBD_COUNT; i++)
      if(event_files[i] > 0 && read(event_files[i], &ev, sizeof(struct input_event)) >= 0){
         if (ev.type == EV_KEY && ev.value < 2){
            switch (ev.code){
               case KEY_Q: event->key = 'q';break;
               case KEY_W: event->key = 'w';break;
               case KEY_E: event->key = 'e';break;
               case KEY_R: event->key = 'r';break;
               case KEY_T: event->key = 't';break;
               case KEY_Y: event->key = 'y';break;
               case KEY_U: event->key = 'u';break;
               case KEY_I: event->key = 'i';break;
               case KEY_O: event->key = 'o';break;
               case KEY_P: event->key = 'p';break;
               case KEY_A: event->key = 'a';break;
               case KEY_S: event->key = 's';break;
               case KEY_D: event->key = 'd';break;
               case KEY_F: event->key = 'f';break;
               case KEY_G: event->key = 'g';break;
               case KEY_H: event->key = 'h';break;
               case KEY_J: event->key = 'j';break;
               case KEY_K: event->key = 'k';break;
               case KEY_L: event->key = 'l';break;
               case KEY_SEMICOLON: event->key = ';';break;
               default: event->key = 0;return false;
            }
            event->pressed = ev.value > 0;
            return true;
         }
         else{
            return false;
         }
      }
   return false;
}
#else
void evkbd_init(){}
void evkbd_deinit(){}
// assumed input file that consists of lines with format:
// scanf("%c;%c;%zu",key,pressed,next_event_in_ticks)
bool evkbd_poll(KeyboardEvent* event){
   static size_t delay = 0;
   bool read_ev = false;
   size_t inbuff_len = 20;

   vTaskDelay(delay);
   char* inbuff = malloc(inbuff_len);
   if(0 < getline(&inbuff,&inbuff_len,stdin)){
      event->key = inbuff[0];
      event->pressed = inbuff[2] == 'p' ? true : false;
   
      delay = atoi(inbuff+4);
      read_ev = true;
   }
   free(inbuff);
   return read_ev;
}
#endif
