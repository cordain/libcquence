#include "cquence.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include "FreeRTOS.h"
#include "cquence_types.h"
#include "libcquence_usr/config.h"
#include "task.h"
#include "timers.h"

#include "sequences.c"
#include "evkbd.c"

#define CHANNEL_COUNT 20

typedef struct {
   size_t a_time;
   size_t d_time;
   size_t r_time;
   uint16_t a_amp;
   uint16_t d_amp;
} envelope_param;

const envelope_param channel_param[CHANNEL_COUNT] = {
   {.a_time =  100U, .a_amp =  10U, .d_time =   50U, .d_amp = 50U, .r_time =    0U },
   {.a_time =  200U, .a_amp =  20U, .d_time =  100U, .d_amp = 50U, .r_time =  100U },
   {.a_time =  300U, .a_amp =  30U, .d_time =   70U, .d_amp = 50U, .r_time =  200U },
   {.a_time =  400U, .a_amp =  40U, .d_time =   30U, .d_amp = 50U, .r_time =  300U },
   {.a_time =  500U, .a_amp =  50U, .d_time =    0U, .d_amp = 50U, .r_time =  400U },
   {.a_time =  600U, .a_amp =  60U, .d_time =   30U, .d_amp = 50U, .r_time =  500U },
   {.a_time =  700U, .a_amp =  80U, .d_time =   70U, .d_amp = 50U, .r_time =  600U },
   {.a_time =  800U, .a_amp = 100U, .d_time =  100U, .d_amp = 50U, .r_time =  700U },
   {.a_time =  900U, .a_amp =  80U, .d_time =   70U, .d_amp = 50U, .r_time =  800U },
   {.a_time = 1000U, .a_amp =  60U, .d_time =   30U, .d_amp = 50U, .r_time =  900U },
   {.a_time = 1100U, .a_amp =  50U, .d_time =    0U, .d_amp = 50U, .r_time = 1000U },
   {.a_time = 1200U, .a_amp =  40U, .d_time =   30U, .d_amp = 50U, .r_time = 1100U },
   {.a_time = 1300U, .a_amp =  30U, .d_time =   70U, .d_amp = 50U, .r_time = 1200U },
   {.a_time = 1400U, .a_amp =  20U, .d_time =  100U, .d_amp = 50U, .r_time = 1300U },
   {.a_time = 1500U, .a_amp =  10U, .d_time =   50U, .d_amp = 50U, .r_time = 1400U },
   {.a_time =    0U, .a_amp =   0U, .d_time =    0U, .d_amp = 50U, .r_time = 1500U },
   {.a_time = 1500U, .a_amp =  10U, .d_time =   50U, .d_amp = 50U, .r_time = 1400U },
   {.a_time = 1400U, .a_amp =  10U, .d_time =   70U, .d_amp = 50U, .r_time = 1400U },
   {.a_time = 1300U, .a_amp =  10U, .d_time =  100U, .d_amp = 50U, .r_time = 1400U },
   {.a_time = 1200U, .a_amp =  10U, .d_time = 2000U, .d_amp = 50U, .r_time = 1400U },
};

void app();

int main(){
   Cquence_Init();
   xTaskCreate(app,"APP",1024,NULL,tskIDLE_PRIORITY+1,NULL);
   vTaskStartScheduler();
}

void app_init(Cquence_OpRequest ads_channels[CHANNEL_COUNT][sizeof(ads)/sizeof(Cquence_OpRequest)], Cquence_OpRequest rel_channels[CHANNEL_COUNT][sizeof(rel)/sizeof(Cquence_OpRequest)], char ads_names[CHANNEL_COUNT][SEQ_NAME_SIZE], char rel_names[CHANNEL_COUNT][SEQ_NAME_SIZE]){
   for(size_t i = 0; i < CHANNEL_COUNT; i++){
      memcpy(ads_channels[i],ads,sizeof(ads));
      memcpy(rel_channels[i],rel,sizeof(rel));
      // rename sequences
      sprintf(ads_names[i],"%s_%ld",ADS_NAME,i);
      sprintf(rel_names[i],"%s_%ld",REL_NAME,i);
      // replace a_amp
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][0].op_request_body.op_request_call.arg)->amp = channel_param[i].a_amp;
      // replace a_time
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][0].op_request_body.op_request_call.arg)->delay = channel_param[i].a_time;
      ads_channels[i][1].op_request_body.op_request_branch.wait_for_ticks = channel_param[i].a_time;
      // replace d_amp
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][2].op_request_body.op_request_call.arg)->amp = channel_param[i].d_amp;
      // replace d_time
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][2].op_request_body.op_request_call.arg)->delay = channel_param[i].d_time;
      ads_channels[i][3].op_request_body.op_request_branch.wait_for_ticks = channel_param[i].d_time;
      // replace s_amp
      ((Cquence_OpCode_AMPLITUDE_SET_Arg*)ads_channels[i][4].op_request_body.op_request_call.arg)->amp = channel_param[i].d_amp;
      // replace r_time
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)rel_channels[i][1].op_request_body.op_request_call.arg)->delay = channel_param[i].r_time;
      rel_channels[i][2].op_request_body.op_request_branch.wait_for_ticks = channel_param[i].r_time;
      // replace channel
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][0].op_request_body.op_request_call.arg)->amp = i;
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)ads_channels[i][2].op_request_body.op_request_call.arg)->amp = i;
      ((Cquence_OpCode_AMPLITUDE_SET_Arg*)ads_channels[i][4].op_request_body.op_request_call.arg)->amp = i;
      ((Cquence_OpCode_AMPLITUDE_LINEAR_Arg*)rel_channels[i][1].op_request_body.op_request_call.arg)->delay = i;
      // replace ads termination source
      ads_channels[i][1].op_request_body.op_request_branch.sender = rel_names[i];
      ads_channels[i][3].op_request_body.op_request_branch.sender = rel_names[i];
      ads_channels[i][5].op_request_body.op_request_receive.sender = rel_names[i];
      // replace rel signal target
      rel_channels[i][0].op_request_body.op_request_send.recipent = ads_names[i];
      // spawn sequences
   }
   
}


void app(){
   Cquence_OpRequest ads_channels[CHANNEL_COUNT][sizeof(ads)/sizeof(Cquence_OpRequest)];
   Cquence_OpRequest rel_channels[CHANNEL_COUNT][sizeof(rel)/sizeof(Cquence_OpRequest)];
   char ads_names[CHANNEL_COUNT][SEQ_NAME_SIZE] = {0};
   char rel_names[CHANNEL_COUNT][SEQ_NAME_SIZE] = {0};
   app_init(ads_channels, rel_channels, ads_names, rel_names);
   //vTaskDelay(100);
   evkbd_init();
   printf("Pick a note\n");
   int quit = 0;

   while (!quit) {
      KeyboardEvent event;
      if (evkbd_poll(&event)) {
#define ADS_OR_R(pressed,channel) pressed?Cquence_NewSequence(ads_names[channel],ads_channels[channel],1):Cquence_NewSequence(rel_names[channel],rel_channels[channel],1)
         switch(event.key){
            case 'q': ADS_OR_R(event.pressed,0);break;
            case 'w': ADS_OR_R(event.pressed,1);break;
            case 'e': ADS_OR_R(event.pressed,2);break;
            case 'r': ADS_OR_R(event.pressed,3);break;
            case 't': ADS_OR_R(event.pressed,4);break;
            case 'y': ADS_OR_R(event.pressed,5);break;
            case 'u': ADS_OR_R(event.pressed,6);break;
            case 'i': ADS_OR_R(event.pressed,7);break;
            case 'o': ADS_OR_R(event.pressed,8);break;
            case 'p': ADS_OR_R(event.pressed,9);break;
            case 'a': ADS_OR_R(event.pressed,10);break;
            case 's': ADS_OR_R(event.pressed,11);break;
            case 'd': ADS_OR_R(event.pressed,12);break;
            case 'f': ADS_OR_R(event.pressed,13);break;
            case 'g': ADS_OR_R(event.pressed,14);break;
            case 'h': ADS_OR_R(event.pressed,15);break;
            case 'j': ADS_OR_R(event.pressed,16);break;
            case 'k': ADS_OR_R(event.pressed,17);break;
            case 'l': ADS_OR_R(event.pressed,18);break;
            case ';': ADS_OR_R(event.pressed,19);break;
            //default: break;
         }
#undef ADS_OR_R
         printf("Key %c %s\n",event.key,event.pressed ? "pressed" : "released");
      }
      //vTaskDelay(1);
   }
   //Cquence_NewSequence(ads_names[i],ads_channels[i],1);
   //Cquence_NewSequence(rel_names[i],rel_channels[i],1);
}

void vAssertCalled(const char * const pcFileName, unsigned long ulLine)
{
        taskENTER_CRITICAL();

        fprintf(stderr, "ASSERT: %s:%lu\n", pcFileName, ulLine);
        exit(-1);

        taskEXIT_CRITICAL();
}

void vLoggingPrintf(const char *pcFormat, ...)
{
        taskENTER_CRITICAL();

        va_list arg;

        va_start( arg, pcFormat );
        vprintf( pcFormat, arg );
        va_end( arg );

        taskEXIT_CRITICAL();
}

