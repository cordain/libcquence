#include "cquence_types.h"
#include "opcodes.h"
#define SEQ1_NAME "seq1"
#define SEQ2_NAME "seq2"

const Cquence_OpRequest seq1[] = {
    CALL(DUMMY,
        .message="SEQ1[0]",
    ),
    CALL(DUMMY,
        .message="SEQ1[1]",
    ),
    CALL(DUMMY,
        .message="SEQ1[2]",
    ),
    CALL(DUMMY,
        .message="SEQ1[3]",
    ),
    END_OF_SEQUENCE()
};

const Cquence_OpRequest seq2[] = {
    CALL(DUMMY,
        .message="SEQ2[0]",
    ),
    CALL(DUMMY,
        .message="SEQ2[1]",
    ),
    CALL(DUMMY,
        .message="SEQ2[2]",
    ),
    CALL(DUMMY,
        .message="SEQ2[3]",
    ),
    END_OF_SEQUENCE()
};
