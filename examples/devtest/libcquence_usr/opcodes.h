///=================================================================================================
/// \\file opcodes.h
///
/// This file contains cofiguration that describes used operations
/// Please respect sections that covers DO NOT CHANGE code
///=================================================================================================

#ifndef LIBCQUENCE_OPCODES_H
#define LIBCQUENCE_OPCODES_H
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
typedef struct{
   const char * message;
} Cquence_OpCode_DUMMY_Arg;


///================================================================================================
/// \\enum  Config_OpCode
/// \\brief Identifiers of operation codes.
///
/// Defined opcodes need to be started from 0 and be left with no empty values,
/// otherwise library will not be working
///================================================================================================
typedef enum{
///=================================================================================================
/// USER OPCODES
   DUMMY,
///=================================================================================================

///=================================================================================================
/// DO NOT CHANGE(len,eos) - standard values used in library
   USER_OP_COUNT,
///=================================================================================================
}Cquence_OpCode;
#endif//LIBCQUENCE_OPCODES_H