///=================================================================================================
/// \file opimpl.c
///
/// This file contains implementations of operations.
/// This file is completely for user usage. It may be ommited and be defined somewhere else.
///=================================================================================================

#include "cquence_types.h"

///=================================================================================================
/// USER OP IMPLEMENTATION
Cquence_OpResult Cquence_DummyOperation(void* arg){
   Cquence_OpCode_DUMMY_Arg* data = arg;
   return (Cquence_OpResult){.successful=true};
}

///=================================================================================================