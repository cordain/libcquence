#!/usr/bin/python3

import re

# define user helper variables
__DUMMY_CALLS_COUNT=8
__SEQ1_COMMANDS_COUNT=5
__SEQ2_COMMANDS_COUNT=5

execution_time = 0.01

def test_equal(test_name,left,right) -> bool:
    print('='*80)
    condition = left == right
    print(test_name)
    print(f"{left}=={right} is {condition}")
    return condition


def execute(output_trace:str) -> bool:
    result = True
    tests = [
        ("check how many dummy operations were called",output_trace.count('DUMMY as seq'),__DUMMY_CALLS_COUNT),
        ("check amount of called operations in SEQ1",output_trace.count('seq1'),__SEQ1_COMMANDS_COUNT),
        ("check amount of called operations in SEQ2",output_trace.count('seq2'),__SEQ2_COMMANDS_COUNT),
    ]
    for test_name,left,right in tests:
        if not test_equal(test_name,left,right):
            result = False
    return result
