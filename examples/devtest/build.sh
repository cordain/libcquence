#!/usr/bin/bash
exit_code=0
target_type="run"
# use script name to change directory where it is put
pushd $(dirname $(realpath $0))
mkdir build
pushd build
if [ "$1" = "test" ]; then
   target_type="test"
fi
cmake -DTARGET_TYPE=$target_type ../
make -j8
if [[ $? -ne "0" ]]; then
   echo "NOT COMPILED"
   exit_code=1
fi
#cp -R ../../src libcquence
#rm -R target
#mkdir target
#SRCCQUENCE=$(find . -type f -name "*.c" | tr -d '\n' | sed "s/\.\// /g")
#SRCFREERTOS="$FREERTOS_PATH/tasks.c $FREERTOS_PATH/queue.c $FREERTOS_PATH/list.c $FREERTOS_PATH/portable/MemMang/heap_1.c $FREERTOS_PATH/portable/ThirdParty/GCC/Posix/port.c"
#INCCQUENCE=$(find -type f -name "*.h" | xargs dirname | sort | uniq | sed "1d" | tr -d '\n' | sed "s/\.\// -I/g")
#INCFREERTOS="-I$FREERTOS_PATH/include -I$FREERTOS_PATH/portable/ThirdParty/GCC/Posix -I./"
#echo gcc $INCCQUENCE $INCFREERTOS $SRCCQUENCE $SRCFREERTOS -o target/$CQUENCE_EXAMPLE
#gcc $INCCQUENCE $INCFREERTOS $SRCFREERTOS $SRCCQUENCE -o target/$CQUENCE_EXAMPLE
popd
rm -R libcquence
rm -R FreeRTOS-Kernel
popd
exit $exit_code
