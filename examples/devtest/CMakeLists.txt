project("libcquence_devtest" C) 

cmake_minimum_required(VERSION 3.5)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_BINARY_DIR}/target)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu11 -O0 -g")

set(FREERTOS_MEMMANG heap_3)
set(FREERTOS_PORT ThirdParty/GCC/Posix)

add_subdirectory($ENV{HOME}/.dev/tools/FreeRTOS-Kernel ${CMAKE_CURRENT_SOURCE_DIR}/FreeRTOS-Kernel)
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../../src ${CMAKE_CURRENT_SOURCE_DIR}/libcquence)

set(TARGET "devtest")

add_executable(
   ${TARGET}
   ${FREERTOS_SRC}
   ${CMAKE_CURRENT_SOURCE_DIR}/libcquence_usr/usr.c
   ${CMAKE_CURRENT_SOURCE_DIR}/libcquence_usr/opimpl.c
   ${LIBCQUENCE_SRC}
   main.c
)

target_include_directories(
   ${TARGET}
   PRIVATE
   ${CMAKE_CURRENT_SOURCE_DIR}
   ${CMAKE_CURRENT_SOURCE_DIR}/libcquence_usr
   ${LIBCQUENCE_INC}
   ${FREERTOS_INC}
)

if(TARGET_TYPE STREQUAL "test")
   add_definitions("-DACCEPTANCE_TESTS")
endif()

target_link_libraries(${TARGET} PUBLIC pthread rt)
